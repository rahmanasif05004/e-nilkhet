<?php

use Illuminate\Database\Seeder;
use App\Models\Delivery;

class CreateSettingDeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Delivery::create([
            'name_en'=>'In Side Dhaka',
            'name_bn'=>'In Side Dhaka',
            'amount'=> 30,
            'status'=> 1,
        ]);

        Delivery::create([
            'name_en'=>'Out Side Dhaka',
            'name_bn'=>'Out Side Dhaka',
            'amount'=> 120,
            'status'=> 1,
        ]);

        Delivery::create([
            'name_en'=>'Local Pick up',
            'name_bn'=>'Local Pick up',
            'amount'=> 0,
            'status'=> 1,
        ]);

    }
}
