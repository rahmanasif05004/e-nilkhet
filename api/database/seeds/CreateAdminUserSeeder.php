<?php

use App\Models\Users\Admin;
use App\Models\Users\User;
use Illuminate\Database\Seeder;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   Admin::truncate();
        User::truncate();
        $admin=new Admin();
        $admin->first_name='admin';
        $admin->last_name='user';
        $admin->save();
        $admin->user()->create([
            'phone'=>'0191292665',
            'email'=>'admin@gmail.com',
            'password'=>'12345'
        ]);

    }
}
