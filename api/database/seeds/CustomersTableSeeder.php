<?php

use App\Models\Users\Customer;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer=new Customer();
        $customer->name=Str::random(10);
        $customer->address= 'House #854, Road 420, Dhaka-1216';
        $customer->area_id=Str::random(2);
        $customer->save();
        $customer->user()->create([
            'phone'=>'01912926553',
            'email'=>Str::random(5).'@gmail.com',
            'password'=>'12345'
        ]);
    }
}
