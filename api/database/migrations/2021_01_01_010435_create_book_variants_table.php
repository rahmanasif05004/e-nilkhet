<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('book_id')->comment('primary key of books table');
            $table->unsignedInteger('variant_id')->comment('primary key of setting_variants table');
            $table->decimal('price', 10,2)->default(0);
            $table->string('discount_type')->default('no')->comment('no,percent,fixed');
            $table->decimal('discount_value',10,2)->default(0)->comment('value calculate depend on discount type');
            $table->unsignedInteger('number_of_page')->default(0);
            $table->unsignedInteger('stock_qty')->default(0);
            $table->unsignedInteger('reorder_qty')->default(10);
            $table->enum('active_status',['Yes','No'])->default('No');
            $table->timestamps();
            // $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_types');
    }
}
