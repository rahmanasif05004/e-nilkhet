<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePackageTableInPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->string('slug')->nullable()->after('name');
            $table->text('summary')->nullable()->after('slug');
            $table->text('additional_info')->nullable()->after('summary');
            $table->string('image')->nullable()->after('additional_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn('slug');
            $table->dropColumn('summary');
            $table->dropColumn('additional_info');
            $table->dropColumn('image');
        });
    }
}
