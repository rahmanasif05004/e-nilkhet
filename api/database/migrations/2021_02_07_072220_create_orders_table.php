<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_number');
            $table->unsignedInteger('customer_id')->comment('primary key of customer table');
            $table->decimal('advance', 10,2)->default(0);
            $table->decimal('discount', 10,2)->default(0)->comment('discount on sub_total');
            // $table->unsignedTinyInteger('discount_type')->nullable()->comment('1=fixed, 2=percent');
            // $table->decimal('discount_percent', 10,2)->default(0)->comment('discount value for percent');
            $table->unsignedInteger('shipping_address_id')->comment('primary key of address table');

            $table->decimal('delivery_charge', 10,2)->default(0);
            $table->decimal('vat', 10,2)->default(0);
            $table->decimal('sub_total', 10,2)->default(0);
            $table->decimal('grand_total', 10,2)->default(0);
            $table->text('note')->nullable()->comment('for authority note');
            $table->unsignedInteger('status_id')->comment('primary key of order_statuses table');
            $table->unsignedInteger('setting_delivery_id')->comment('primary key of deliveries type table table')->nullable();
            $table->unsignedInteger('created_by')->comment('primary key of users table');
            $table->unsignedInteger('updated_by')->comment('primary key of users table')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
