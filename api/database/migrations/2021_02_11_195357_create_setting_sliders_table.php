<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_sliders', function (Blueprint $table) {
            $table->id();
            $table->string('title_1', 255)->nullable();
            $table->string('title_2', 255)->nullable();
            $table->string('title_3', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_sliders');
    }
}
