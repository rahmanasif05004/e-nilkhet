<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddresseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropForeign(['setting_countrie_id','setting_division_id']);
            $table->dropColumn(['setting_countrie_id','setting_division_id']);
            $table->unsignedInteger('user_id')->after('id');
            $table->string('type', 32)->default('home');
            $table->unsignedInteger('setting_zone_id')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn(['user_id','type','setting_zone_id']);
            $table->unsignedInteger('setting_countrie_id');
            $table->unsignedInteger('setting_division_id');
            $table->foreign('setting_countrie_id')->references('id')->on('setting_countries');
            $table->foreign('setting_division_id')->references('id')->on('setting_divisions');
        });
    }
}
