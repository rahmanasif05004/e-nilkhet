<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookSettingSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_setting_subjects', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('book_id')->comment('primary key of books table');
            $table->unsignedInteger('subject_id')->comment('primary key of setting_subjects table');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_setting_subjects');
    }
}
