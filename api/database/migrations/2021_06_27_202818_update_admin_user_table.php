<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdminUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_user', function (Blueprint $table) {
            $table->unsignedBigInteger('profession_id')->nullable();
            $table->string('name')->nullable();
            $table->text('address')->nullable();
            $table->unsignedBigInteger('area_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_user', function (Blueprint $table) {
            $table->dropColumn('profession_id');
            $table->dropColumn('name');
            $table->dropColumn('address');
            $table->dropColumn('area_id');
        });
    }
}
