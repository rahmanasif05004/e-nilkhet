<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('book_category_id')->comment('primary key of book_categories');
            $table->unsignedInteger('sort')->comment('sorting order');
            $table->string('slug');
            $table->string('title_en');
            $table->string('title_bn')->nullable();
            $table->text('tags')->nullable()->comment('comma seprated book tags name');
            $table->string('edition')->nullable();
            $table->string('series')->nullable();
            $table->string('publishing_year')->nullable();
            $table->string('isbn_number')->nullable();
            $table->unsignedInteger('origin_country_id')->nullable()->comment('primary key of countries table');
            // setting
            $table->string('author_id')->nullable()->comment('primary key of setting_authors table');
            $table->string('publisher_id')->nullable()->comment('primary key of setting_publishers table');
            $table->string('translator_id')->nullable()->comment('primary key of setting_translators table');
            $table->string('editor_id')->nullable()->comment('primary key of setting_editors table');
            // others
            $table->text('summary_en')->nullable();
            $table->text('summary_bn')->nullable();
            $table->text('additional_info')->nullable();
            $table->text('video_link')->nullable();
            $table->text('youtube_link')->nullable();
            // status
            $table->string('best_book')->default('no');
            $table->string('special_book')->default('no');
            $table->string('featured_book')->default('no');
            $table->string('available')->default('no');
            $table->string('image')->nullable();
            $table->unsignedInteger('created_by')->comment('primary key of admin table');
            $table->unsignedInteger('updated_by')->comment('primary key of admin table');
            $table->timestamps();
            // $table->foreign('book_category_id')->references('id')->on('book_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
