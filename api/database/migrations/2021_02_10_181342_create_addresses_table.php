<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->text('address')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->unsignedBigInteger('setting_countrie_id')->comment('Primary key of setting_countries table');
            $table->unsignedBigInteger('setting_division_id')->comment('Primary key of setting_divisions table');
            $table->unsignedBigInteger('setting_district_id')->comment('Primary key of setting_districts table');
            $table->unsignedBigInteger('setting_area_id')->comment('Primary key of setting_areas table');

            $table->foreign('setting_countrie_id')->references('id')->on('setting_countries');
            $table->foreign('setting_division_id')->references('id')->on('setting_divisions');
            $table->foreign('setting_district_id')->references('id')->on('setting_districts');
            $table->foreign('setting_area_id')->references('id')->on('setting_areas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
