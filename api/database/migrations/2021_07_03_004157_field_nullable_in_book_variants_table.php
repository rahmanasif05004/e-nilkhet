<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FieldNullableInBookVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_variants', function (Blueprint $table) {
            $table->string('discount_type')->nullable()->change();
            $table->decimal('discount_value', 10, 2)->nullable()->change();
            $table->integer('number_of_page')->nullable()->change();
            $table->integer('stock_qty')->nullable()->change();
            $table->integer('reorder_qty')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_variants', function (Blueprint $table) {
            $table->string('discount_type')->nullable(false)->change();
            $table->decimal('discount_value', 10, 2)->nullable(false)->change();
            $table->integer('number_of_page')->nullable(false)->change();
            $table->integer('stock_qty')->nullable(false)->change();
            $table->integer('reorder_qty')->nullable(false)->change();
        });
    }
}
