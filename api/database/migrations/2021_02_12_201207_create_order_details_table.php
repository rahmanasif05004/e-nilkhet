<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('order_id')->nullable()->comment('primary key of orders table');
            $table->unsignedInteger('book_variant_id')->comment('primary key of book_variants table');
            $table->decimal('price', 10,2)->default(0);
            $table->unsignedInteger('qty')->default(0);
            $table->decimal('total', 10,2)->default(0);
            // $table->decimal('discount', 10,2)->default(0)->comment('any discount for this item');
            // $table->unsignedTinyInteger('discount_type')->nullable()->comment('1=fixed, 2=percent');
            // $table->decimal('discount_percent', 10,2)->default(0)->comment('discount value for percent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
