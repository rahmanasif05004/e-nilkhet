<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Page')->group(function(){
    Route::post('pages/image-uploads','PageController@imageUpload');
    Route::resource('pages','PageController');
});