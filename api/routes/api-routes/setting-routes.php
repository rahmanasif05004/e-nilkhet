<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Settings')->group(function(){
    Route::post('setting-authors/image-uploads','SettingAuthorController@imageUpload');
    Route::delete('setting-authors/{id}/remove-image','SettingAuthorController@removeImage');
    Route::resource('setting-variants','SettingVariantController');
    Route::resource('setting-translators','SettingTranslatorController');
    Route::resource('setting-subjects','SettingSubjectController');
    Route::resource('setting-publishers','SettingPublisherController');
    Route::resource('setting-languages','SettingLanguageController');
    Route::resource('setting-editors','SettingEditorController');
    Route::resource('setting-divisions','SettingDivisionController');
    Route::resource('setting-districts','SettingDistrictController');
    Route::resource('setting-countries','SettingCountryController');
    Route::resource('setting-authors','SettingAuthorController');
    Route::resource('setting-areas','SettingAreaController');
    Route::resource('setting-sliders','SettingSliderController');
    Route::resource('setting-zones','SettingZoneController');
    Route::resource('categories','BookCategoryController');
});