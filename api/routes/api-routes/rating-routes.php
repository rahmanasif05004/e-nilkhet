<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Rating')
	->middleware(['auth:sanctum'])
	->group(function(){
	    Route::resource('ratings','RatingController');
	});