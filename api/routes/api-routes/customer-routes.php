<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::namespace('Customer')->group(function () {
    Route::get('get-customer','CustomerController@getCustomer');
    Route::post('save-customer','CustomerController@saveCustomer');
    Route::post('customer-info-update','CustomerController@customerInfoUpdate');
    Route::resource('customers','CustomerController');
    Route::resource('professions','ProfessionController');
    Route::resource('customer-groups','CustomerGroupController');
    Route::resource('addresses','AddressController');
});

Route::namespace('Customer')
    ->middleware(['auth:sanctum'])
    ->group(function () {
    Route::resource('addresses','AddressController');
});


?>