<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Package')->group(function(){
    Route::post('packages/image-upload','PackageController@imageUpload');
    Route::resource('packages','PackageController');
});


?>