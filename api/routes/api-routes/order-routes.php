<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Order')
    ->middleware(['auth:sanctum'])
    ->group(function(){
    Route::get('return-orders','OrderController@returnOrders');
    Route::post('save-offline-sales','OrderController@saveOfflineSales');
    Route::post('order-save','OrderController@saveOrder');
    Route::resource('orders','OrderController');
});


?>