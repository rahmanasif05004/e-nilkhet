<?php


use Illuminate\Support\Facades\Route;

Route::namespace('Authentication')->group(function(){
    Route::post('customer-login','LoginController@customerLogin');
    Route::post('customer-register','RegistrationController@customerRegister');
    Route::post('admin-login','LoginController@adminLogin');
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('me','LoginController@me');
        Route::post('logout','LoginController@logout');
    });
});