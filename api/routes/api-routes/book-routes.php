<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Book')->group(function(){
    
    Route::get('book-variants','BookController@getBookVariants');
    Route::post('save-book-variants-discount','BookController@saveBookVariantsDiscount');

    Route::post('books/image-upload','BookController@imageUpload');
    Route::resource('books','BookController');
    Route::get('book-deatails/{slug}','BookController@getBookInfo');
    Route::resource('book-categories','BookCategoryController');
    
    //change this route farther 
    Route::post('book-info','BookController@getBookInfoByWishList');
    Route::post('books/bulk-product-csv-upload','ProductBulkInsertionController@csvFileUpload');
    
});