<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Discount')
    ->middleware(['auth:sanctum'])
    ->group(function(){
    Route::get('active-discounts','DiscountController@activeDiscount');
    Route::resource('discounts','DiscountController');
    Route::resource('customer-discounts','CustomerDiscountController');
});


?>