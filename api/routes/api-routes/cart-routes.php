<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::namespace('Cart')->group(function () {
    Route::resource('carts','CartController');
});


?>