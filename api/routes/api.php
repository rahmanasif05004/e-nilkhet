<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('')->get("/v1/me/", function (Request $request) {
    return $request->user();
});



Route::prefix('v1/')->group(function(){
    Route::namespace('Frontend')->group(function () {

        Route::get('book-parent-category-wise','BookFrontInformationController@getParentCategoryWiseProduct');
        Route::get('book-menu-categories','MenuController@getBookMenuCategory');
        Route::get('sticky-menu-categories/{category_slug}','MenuController@getStickyMenuCategory');
        Route::get('books-preorder-list','BookFrontInformationController@getPreOrderBook');
        Route::get('books-package-list','BookFrontInformationController@getPackageProductList');
        Route::get('book-package-list-by-slug/{slug}','BookFrontInformationController@getPackageWiseBook');
        Route::get('books-list/{category_slug?}','BookFrontInformationController@getCategoryWiseBook');
        Route::get('sub-menu-product/{type}/{id}','BookFrontInformationController@getSubMenuWiseBook');
        Route::get('shop-product/{name?}/{id?}','BookFrontInformationController@getProductDefaultShopProduct');
        Route::get('sub-menu-item','MenuController@getSubMenuList');
        Route::post('cart-products','CartController@getCartItemProduct');
        Route::post('cart-info-operation','CartController@checkCartProduct');
        Route::middleware(['auth:sanctum'])->group(function () {
            Route::get('get-cart-info','CartController@getAuthenticateUserCartData');
            Route::post('cart-authenticate-info-operation','CartController@checkCartProduct');
        });
        
    });
    // Route::post('customer-login',[LoginController::class,'customerLogin']);
    // Route::post('admin-login',[LoginController::class,'adminLogin']);

    Route::prefix('common')->group(function () {
        Route::get('load-sliders','CommonController@loadSliders');
        Route::get('load-book-categories','CommonController@loadBookCategories');
    });
    

    
});

// Route::namespace('Settings')->group(function () {
//     Route::resource('setting-variants','SettingVariantController');
//     Route::resource('setting-translators','SettingTranslatorController');
//     Route::resource('setting-subjects','SettingSubjectController');
//     Route::resource('setting-publishers','SettingPublisherController');
//     Route::resource('setting-languages','SettingLanguageController');
//     Route::resource('setting-editors','SettingEditorController');
//     Route::resource('setting-divisions','SettingDivisionController');
//     Route::resource('setting-districts','SettingDistrictController');
//     Route::resource('setting-countries','SettingCountryController');
//     Route::resource('setting-authors','SettingAuthorController');
//     Route::resource('setting-areas','SettingAreaController');
//     Route::resource('categories','BookCategoryController');
// });

// Route::namespace('Product')->group(function () {
//     Route::resource('products','SettingVariantController');
// });

// Route::namespace('Book')->group(function () {
//     Route::resource('books','BookController');
//     Route::resource('book-categories','BookCategoryController');
//     Route::get('book-deatails/{slug}','BookController@getBookInfo');
// });

// Route::namespace('Customer')->group(function () {
//     Route::resource('customers','CustomerController');
// });


Route::namespace('Order')->group(function () {
    Route::resource('orders','OrderController');
});