<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'admin'=>\App\Models\Users\Admin::class,
            'customer'=>\App\Models\Users\Customer::class,
            'package'=>\App\Models\Discount\Package::class,
            'customer_group'=>\App\Models\Users\CustomerGroup::class,
        ]);
    }
}
