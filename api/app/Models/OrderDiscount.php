<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDiscount extends Model
{
    protected $table = 'order_discounts';
    protected $fillable = ['order_id','type','discount_id','amount_fixed','amount_parcentage'];
}
