<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['parent_id','name_en','name_bn','slug','status'];

    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }

   

    

    public function setNameEnAttribute($value=''){
        $this->attributes['name_en'] = trim($value);
        $this->attributes['slug'] = $this->slugify($value);
    }

    private function slugify($value){
        $value = strtolower(str_replace(' ','-',$value));
        return $value;
    }

    public function books(){
        return $this->hasMany(Book::class, 'book_category_id');
    }

    public function child() {
        return $this->hasMany('App\Models\Category','parent_id','id')->select('id','name_en','parent_id','name_bn', 'slug') ;
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
        });

        static::updating(function($model){
        });
    }
}
