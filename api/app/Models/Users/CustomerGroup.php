<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book;
use App\Models\Discount\Discount;

class CustomerGroup extends Model
{
    protected $fillable = ['name','status'];

    public function customers(){
        return $this->belongsToMany(Customer::class);
    }

    public function discount(){
        return $this->hasOne(Discount::class,'type_id')->whereType('customer_group');
    }

}
