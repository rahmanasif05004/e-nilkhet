<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\User;
class Admin extends Model
{
    protected $table='admin_user';
    protected $fillable=[
        'first_name',
        'last_name',
    ];
    protected $appends = ['name'];

    public function getNameAttribute(){
        return $this->first_name .' '.$this->last_name;
    }
    public function user()
    {
        return $this->morphOne(User::class,'userable');
    }
}
