<?php

namespace App\Models\Users;

use App\Models\Discount\CustomerDiscount;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Discount\Discount;
use Laravel\Sanctum\HasApiTokens;
use Hash;
use DB;

class User extends Authenticatable
{

    use HasApiTokens, Notifiable;
    const IS_ACTIVE='Yes';
    const ADMIN='admin';
    const CUSTOMER='customer';
    protected $fillable = [
        'userable_id',
        'userable_type',
        'phone',
        'email',
        'profile_image',
        'password',
        
    ];

    protected $with=['userable'];

    protected $guarded=['is_active'];

    protected $hidden=[
        'password'
    ];

    protected $appends = ['full_name','discount'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute(){
        return $this->userable->first_name .' '. $this->userable->last_name;
    }
    

    public static function boot()
    {
        parent::boot();
        static::saving(function($model){
            Hash::make($model->password);
            if(request()->password != $model->getOriginal('password') || request()->password !== '')
            {
                $model->password = Hash::make($model->password);
            }
            else
            {
                unset($model->password);
            }
        });
    }

    public function userable()
    {
        return $this->morphTo();
    }

    public function getDiscountAttribute()
    {
        $discount = 0;
        if($this->userable_type == 'customer'){
            $customerDiscount = CustomerDiscount::where('customer_id',$this->userable_id)->first();
            if($customerDiscount){
                $discount = $customerDiscount->value;
            }
        }
        return $discount;
    }

}
