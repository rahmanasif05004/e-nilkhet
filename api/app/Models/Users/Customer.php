<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\User;
use App\Models\Order\Order;
class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = ['name','address','phone','area_id'];

    public function area() 
    {
        return $this->hasOne(SettingArea::class,'area_id') ;
    }

    public function user()
    {
        return $this->morphOne(User::class,'userable');
    }

    public function orders()
    {
        return $this->hasMany(Order::class,'customer_id');
    }

    public function customerGroups()
    {
        return $this->belongsToMany(CustomerGroup::class);
    }

}
