<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingZone extends Model
{
    protected $table = 'setting_zones';
    protected $fillable = ['setting_area_id','name_en','name_bn','status'];

    public function area()
    {
        return $this->belongsTo(SettingArea::class,'setting_area_id')->select('name_en','id');
    }

    // public static function boot()
    // {
    //     parent::boot();
    //     static::creating(function($model){
    //     });

    //     static::updating(function($model){
    //     });
    // }
}
