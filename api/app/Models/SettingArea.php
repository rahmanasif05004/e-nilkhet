<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SettingDistrict;
class SettingArea extends Model
{
    protected $table = 'setting_areas';
    protected $fillable = ['district_id','name_en','name_bn','status'];

    public function district()
    {
        return $this->hasOne(SettingDistrict::class,'id','district_id')->select('name_en','id');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
        });

        static::updating(function($model){
        });
    }
}
