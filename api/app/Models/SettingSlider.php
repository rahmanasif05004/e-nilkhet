<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingSlider extends Model
{
    protected $table = 'setting_sliders';
    protected $fillable = ['title_1','title_2','title_3','image','status'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
        });

        static::updating(function($model){
        });
    }
}
