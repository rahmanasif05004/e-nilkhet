<?php

namespace App\Models\Discount;
use App\Models\Book;
use App\Models\Users\Customer;
use Illuminate\Database\Eloquent\Model;

class CustomerDiscount extends Model
{

    protected $table = 'customer_discounts';

    protected $fillable = ['customer_id','discount_type','value','status','created_by'];

    public function customer(){
    	return $this->belongsTo(Customer::class);
    }
}
