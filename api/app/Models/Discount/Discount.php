<?php

namespace App\Models\Discount;
use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = ['title','note','discountable_type','discountable_id','discount_type','value','created_by','started_at','ended_at','status','is_priority'];

    public function books(){
    	return $this->belongsToMany(Book::class);
    }

    public function discountable()
    {
        return $this->morphTo()->withDefault();
    }
}
