<?php

namespace App\Models\Discount;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book;

class Package extends Model
{
    protected $fillable = [
        'title_en',
        'title_bn',
        'slug',
        'summary',
        'additional_info',
        'image',
        'status'
    ];

    public function setNameAttribute($value){
        $this->attributes['name'] = trim($value);
        $this->attributes['slug'] = $this->slugify($value);
    }

    private function slugify($value){
        $value = strtolower(str_replace(' ','-',$value));
        return $value;
    }

    public function books(){
    	return $this->belongsToMany(Book::class);
    }

    public function discount()
    {
        return $this->morphOne(Discount::class, 'discountable');
    }
}
