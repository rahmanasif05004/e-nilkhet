<?php

namespace App\Models;
use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
    protected $table= 'book_category';
    protected $fillable = [
        'category_id',
        'book_id'
    ];

    public function books()
    {
        return $this->belongsTo(Book::class,'book_id');
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }

}
