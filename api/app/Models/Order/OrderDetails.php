<?php

namespace App\Models\Order;

use App\Models\Order\Order;
use App\Models\BookVariant;
use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
	protected $table = 'order_details';
    protected $fillable = [
    	'order_id',
        'book_variant_id',
        'price',
        'qty',
        'total'
    ];

    public function order(){
    	return $this->belongsTo(Order::class,'order_id')->withDefault();
    }

    public function variant(){
        return $this->belongsTo(BookVariant::class,'book_variant_id')->withDefault();
    }

    /**
     * Set the total value after calculation.
     *
     * @param  string  $value
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($model){

            //  $this->total = $model->price * $model->qty;
        });

        static::updating(function($model){
        });
    }

}
