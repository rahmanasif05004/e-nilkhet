<?php

namespace App\Models\Order;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class OrderReturn extends Model
{
    protected $fillable = [
        'order_id',
        'return_amount',
        'deduct_amount',
        'created_by',        
    ];

    public function user(){
        return $this->belongsTo(User::class, 'created_by')->withDefault();
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id')->withDefault();
    }

    public function getCreatedAtAttribute($value){
        return date('M-d-Y h:i a', strtotime($value));
    }

}
