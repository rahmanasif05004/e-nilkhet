<?php

namespace App\Models\Order;
use App\Models\Order\Order;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = 'order_statuses';

    protected $fillable = [
    	'date',
        'order_id',
        'status_id',
        'note'
    ];

    public function order(){
    	return $this->belongsTo(Order::class,'order_id')->withDefault();
    }

}
