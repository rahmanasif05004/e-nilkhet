<?php

namespace App\Models\Order;
use App\Models\Users\Customer;
use App\Models\Order\OrderStatus;
use App\Models\Order\OrderDetails;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{	
	protected $table = 'orders';
    protected $fillabel = [
    		'customer_id',
            'advance',
            'discount',
            'delivery_charge',
            'vat',
            'note',
            'sub_total',
            'grand_total',
            'setting_delivery_id',
            'shipping_address_id',
            'status_id',
            'created_by',
            'updated_by'
    ];
    protected $appends = ['totalAmount'];

    public function getTotalAmountAttribute(){
        $total = 0;
        if($this->orderDetails){
            foreach($this->orderDetails as $item){
                $total += $item->price * $item->qty;
            }
        }
        return $total;
    }

    public function getCreatedAtAttribute($value){
        return date('M-d-Y', strtotime($value));
    }

    public function customerDetails(){
        return $this->hasOne(Customer::class,'id', 'customer_id');
    }

    public function orderDetails(){
        return $this->hasMany(OrderDetails::class, 'order_id');
    }

    public function orderStatus(){
        return $this->hasMany(OrderStatus::class, 'order_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by')->withDefault();
    }

    
}
