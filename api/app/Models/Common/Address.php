<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use App\Models\SettingDistrict;
use App\Models\SettingArea;
use App\Models\SettingZone;

class Address extends Model
{
    protected $fillable = [
    	'user_id',
        'name',
        'phone',
        'address',
        'setting_area_id',
        'setting_district_id',
        'type',
        'setting_zone_id'
    ];

    public function district(){
        return $this->belongsTo(SettingDistrict::class, 'setting_district_id')->withDefault();
    }

    public function area(){
        return $this->belongsTo(SettingArea::class, 'setting_area_id')->withDefault();
    }

    public function zone(){
        return $this->belongsTo(SettingZone::class, 'setting_zone_id')->withDefault();
    }
}
