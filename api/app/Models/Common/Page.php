<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title','slug','content','type','photo','is_active'];

    public $timestamps = true;
}
