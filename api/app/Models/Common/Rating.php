<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\User;

class Rating extends Model
{
    protected $fillable = ['user_id','book_id','rating','content','is_active'];

    public $timestamps = true;

    public function user(){
    	return $this->belongsTo(User::class, 'user_id');
    }
}
