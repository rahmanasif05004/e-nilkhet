<?php

namespace App\Models;
use DB;
use App\Models\BookVariant;
use App\Models\Category;
use App\Models\Discount\Package;
use App\Models\Users\CustomerGroup;
use App\Models\Discount\Discount;
use App\Models\Common\Rating;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected $fillable = [
    		'sort',
    		'slug',
    		'title_en',
    		'title_bn',
    		'tags',
            'edition',
            'series',
            'publishing_year',
            'isbn_number',
            'origin_country_id',
            // setting
            'author_id',
            'publisher_id',
            'translator_id',
            'editor_id',
            // others
            'summary_en',
            'summary_bn',
            'additional_info',
            'video_link',
            'youtube_link',
            // status
            'best_book',
            'special_book',
            'featured_book',
            'available',
            'is_preorder',
            'image',
            'created_by',
            'updated_by',
    	];
    

    protected $appends = ['discountAmount'];

    public function getDiscountAmountAttribute(){
        $discountData = null;
        $discountIds = [];
        $currentBookDiscounts = DB::table('book_discount')
            ->whereBookId($this->id)
            ->get()
            ->toArray();
        
        if(count($currentBookDiscounts)){
            $discountIds = array_unique(array_column($currentBookDiscounts, 'discount_id'));
        }
        $currentPackageDiscounts = DB::table('book_package')
            ->whereBookId($this->id)
            ->get()
            ->toArray();
        if(count($currentPackageDiscounts)){
            $packageIds = array_unique(array_column($currentPackageDiscounts, 'package_id'));
            $packageDiscountIds = Discount::whereStatus(1)
                ->whereDiscountableType('package')
                ->whereIn('discountable_id', $packageIds)
                ->where(
                    function($query) {
                        $query->whereNull('started_at');
                        $query->orWhere(function($chilsQuery){
                            $chilsQuery->where('started_at', '<', date('Y-m-d h:i:s'));  
                            $chilsQuery->orWhere('ended_at', '>', date('Y-m-d h:i:s'));  
                        });
                })
                ->pluck('id');
            // $finalDiscounsIds = array_merge($discountIds, $packageDiscountIds->to);            
            if(count($packageDiscountIds)){
                $discountIds = array_merge($discountIds, $packageDiscountIds->toArray());
            }
        }
        
        $discounts = Discount::whereIn('id', $discountIds);
        if($discounts->count() > 1){
            $discounts = $discounts->whereIsPriority(1);
        }
        $discount = $discounts->first();
        if($discount){
            $discountData = $discount;
        }

        return $discountData;
        
    }

    public function setTitleEnAttribute($value=''){
        $this->attributes['title_en'] = trim($value);
        $this->attributes['slug'] = $this->slugify($value);
    }

    private function slugify($value){
        $value = strtolower(str_replace(' ','-',$value));
        return $value;
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }

    public function variants(){
    	return $this->hasMany(BookVariant::class);
    }

    public function activeVariant(){
    	return $this->belongsTo(BookVariant::class,'id','book_id')->where('active_status','=','Yes');
    }


    /**
     * Make Relationship For get Country And Id  .
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function country()
    {
        return $this->belongsTo(SettingCountry::class,'origin_country_id')->select('id','name_en','name_bn');
    }

    /**
     * Make Relationship For get Author Name  And Id  .
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function author()
    {
        return $this->belongsTo(SettingAuthor::class,'author_id');
    }

    /**
     * Make Relationship For get Author Name  And Id  .
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function publisher()
    {
        return $this->belongsTo(SettingPublisher::class,'publisher_id')->select('id','name_en','name_bn');
    }

    /**
     * Make Relationship For get Author Name  And Id  .
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function translator()
    {
        return $this->belongsTo(SettingTranslator::class,'translator_id')->select('id','name_en','name_bn');
    }

    /**
     * Make Relationship For get Author Name  And Id  .
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function editor()
    {
        return $this->belongsTo(SettingEditor::class,'editor_id')->select('id','name_en','name_bn');
    }

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function packages(){
        return $this->belongsToMany(Package::class);
    }

    public function customerGroups(){
        return $this->belongsToMany(CustomerGroup::class);
    }


    public function ratings(){
        return $this->hasMany(Rating::class, 'book_id')->where('is_active', 1);
    }

}
