<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SettingDivision;
class SettingDistrict extends Model
{
    protected $table = 'setting_districts';
    protected $fillable = ['division_id','name_en','name_bn','status'];

    public function division()
    {
        return $this->hasOne(SettingDivision::class,'id','division_id')->select('name_en','id');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
        });

        static::updating(function($model){
        });
    }
}
