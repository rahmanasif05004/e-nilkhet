<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingAuthor extends Model
{
    protected $table = 'setting_authors';
    protected $fillable = ['name_en','name_bn','description','image','status'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
        });

        static::updating(function($model){
        });
    }
}
