<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookVariant extends Model
{
    protected $fillable = [
        'book_id',
        'variant_id',
        'price',
        'discount_type',
        'discount_value',
        'number_of_page',
        'stock_qty',
        'reorder_qty',
        'active_status'
    ];

    

    /**
     * Make Relationship For get Author Name  And Id  .
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function variantInfo()
    {
        return $this->belongsTo(SettingVariant::class,'variant_id','id')->select('id','name_en','name_bn');
    }

    /**
     * Make Relationship For get Author Name  And Id  .
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function book()
    {
        return $this->belongsTo(Book::class,'book_id')->withDefault();
    }

    /**
     * Cart Book Relation for selected data
     *
     * @return \Illuminate\Http\Response
     * @author IAR
     */
    public function bookCartInfo()
    {
        return $this->belongsTo(Book::class,'book_id')->select('title_bn','title_en','slug','id');
    }
}
