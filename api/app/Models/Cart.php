<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Cart extends Model
{
    protected $fillable = ['user_id','product_id','variant_id','quantity'];

    public function bookVariant(){
        return $this->belongsTo(BookVariant::class, 'variant_id');
    }


    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
            $model->user_id = Auth::user()->userable_id ;
        });

        static::updating(function($model){
        });
    }
}
