<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingDivision extends Model
{
    protected $table = 'setting_divisions';
    protected $fillable = ['name_en','name_bn','status'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model){
        });

        static::updating(function($model){
        });
    }
}
