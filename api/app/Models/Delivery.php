<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'setting_deliveries';
    protected $fillable = ['name_en','name_bn','amount','status'];
}
