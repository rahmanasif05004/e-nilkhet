<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Traits\FileUpload;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Res;
use App\Traits\ResponseJsonAble;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\PageRequest;
use App\Models\Common\Page;
use DB;

class PageController extends Controller
{

    use ResponseJsonAble, FileUpload;
    /**
     * @var variant
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Page $page)
    {
        $this->model = $page ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $pages =$this->model->query();
        if($request->input('for')){
            $pages->whereType($request->input('for'));
        }
        $data['pages'] = $pages->get();
        return $this->respondCreated('Page List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        try{
            $data = collect($request->only($this->model->getModel()->getFillable()));
            $slug = \Str::slug($request->title);
            $data = $data->merge(['slug'=> $slug]);
            $data['page'] = $this->model->create($data->toArray());
            return $this->respondCreated('Page Successfully Created', $data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        try
        {
            $data['page'] = $this->model->whereSlug($slug)->first();
            return $this->respondCreated('Page Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['page'] = $this->model->find($id);
           return $this->respondCreated('Book Category Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $page  = $this->model->findOrFail($id);
            $data['page'] = $page->update($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Page Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $page = $this->model->findOrFail($id);
            if($page->delete()){
                return $this->respondCreated('page Successfully Deleted');
            }else{
                return $this->respondNotFound('page Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Something went wrong!');
        }
    }

    public function imageUpload(Request $request){
        try 
        {
            $file = $request->file('file');
            $image =  $this->saveFormationSingleImage($file, 'pages');
            return $this->respondCreated('Pages Image Uploaded', $image);
        } 
        catch (Exception $e) 
        {
            return $this->respondNotFound('Sorry Operation Failed');
        }
    }
}
