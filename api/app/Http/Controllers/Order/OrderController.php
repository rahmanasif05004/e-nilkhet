<?php

namespace App\Http\Controllers\Order;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as Res;
use App\Traits\ResponseJsonAble;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\Order\OrderRequest;
use App\Models\BookVariant;
use App\Models\Users\User;
use App\Models\Order\Order;
use App\Models\Order\OrderDetails;
use App\Models\Order\OrderReturn;
use App\Models\Users\Customer;
use App\Models\Discount\Discount;
use App\Models\Discount\CustomerDiscount;
use App\Models\Cart;
use App\Models\OrderDiscount;
use App\Models\Delivery;

class OrderController extends Controller
{
    use ResponseJsonAble;

    /**
     * @var variant
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->model = $order ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $orders = Order::query()->with(['customerDetails','orderDetails','orderStatus','user.userable']);
        if($request->userId){
            $user = User::findOrFail($request->userId);
            $orders = $orders->where('customer_id', $user->userable_id);
        }
        $data['orders'] = $orders->get();
        return Response::json($data, Res::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data, Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $fillableData = $request->only($this->model->getModel()->getFillable());
            $fillableData['created_by'] = 1;
            $fillableData['updated_by'] = 1;
            $data['order'] = $this->model->create($fillableData);
            if ($request->variant) {
                foreach ($request->variant as $key => $value) {
                    $orderVariant = new OrderDetails();
                    $orderVariantFillableData = collect($value)->only($orderVariant->getFillable())->toArray();
                    $orderVariantFillableData['customer_id'] = 1;
                    $orderVariantFillableData['order_id'] = $data['order']->id;
                    $orderVariant->fill($orderVariantFillableData)->save();
                }
            }
            DB::commit();
            return $this->respondCreated('Order Successfully Created', $data);
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data['orders'] = $this->model->with('orderDetails.variant.book','customerDetails')->findOrFail($id);
            return $this->respondCreated('Order Successfully Get', $data);
        } catch (ModelNotFoundException $e) {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $order = $this->model->findOrFail($id);
            $itemList = [];
            foreach($order->orderDetails as $key => $details){
                $itemList[$key]['varinatId'] = $details->book_variant_id;
                $itemList[$key]['bookName'] = $details->variant->book->title_en;
                $itemList[$key]['variantName'] = $details->variant->variantInfo->name_en;
                $itemList[$key]['price'] = $details->price;
                $itemList[$key]['qty'] = $details->qty;
            }
            $data['customer_id'] = $order->customer_id;
            $data['discount'] = $order->discount;
            $data['itemList'] = $itemList;
            return $this->respondCreated('Order Successfully Get', $data);
        } catch (ModelNotFoundException $e) {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        try {
            if($request->orderType == 'edit'){
                DB::beginTransaction();
                if($request->discount > 0){
                    $order->discount = $request->discount;
                    $order->save();
                }
                
                if(count($request->itemList)){
                    $orderDetailsData = [];
                    foreach($request->itemList as $key => $item){
                        $bookVariant = BookVariant::findOrFail($item['varinatId']);
                        $orderDetailsData[$key]['order_id'] = $id;
                        $orderDetailsData[$key]['book_variant_id'] = $bookVariant->id;
                        $orderDetailsData[$key]['price'] = $item['price'];
                        $orderDetailsData[$key]['qty'] = $item['qty'];
                    }
                    $order->orderDetails()->delete();
                    OrderDetails::insert($orderDetailsData);
                }

                DB::commit();
                return $this->respondCreated('Order Successfully Updated');
            }elseif($request->orderType == 'return'){
                $orderReturn = new OrderReturn();
                $orderReturn->order_id = $order->id;
                $orderReturn->return_amount = ($order->totalAmount - $request->discount) - $request->deduct_amount?:0;
                $orderReturn->deduct_amount = $request->deduct_amount?:0;
                $orderReturn->created_by = Auth::user()->id;
                $orderReturn->save();
            }else{
                return $this->respondInternalError('Sorry, Operation Failed');
            }
            

        } catch (\Throwable $th) {
            return $th;
            DB::rollBack();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $bookInfo = $this->model->with('orderDetails')->findOrFail($id);
            if ($bookInfo->delete()) {
                return $this->respondCreated('Order Successfully Deleted');
            } else {
                return $this->respondNotFound('Order Not Deleted');
            }
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound('Order Not Deleted');
        }
    }

    /**
     * Order Request recive Customer information with some other information 
     * getNextOrderNumber() generate new invoice_code for order 
     * getDeliveryAmount() return current order delivery amount depend or request delivery type property
     * createOrderDetailsInfo() create individual item react in order_details table with item summation 
     * customerTotalDiscount() return customer current all discount and create order discount for customer 
     * @param Request $request Request
     *
     * @return Response JSON
     * @throws Exception
     * @author IAR
     * @Rest\Post("/save-order")
     */
    public function saveOrder(Request $request)
    {
        
        DB::beginTransaction();
        try {

            $currentUser = User::findOrFail(Auth::user()->id);
            // $customer = $currentUser->userable;
            // $customer->name = $request->customer['firstName'].' '.$request->customer['lastName'];
            // $customer->address = $request->customer['address'];
            // $customer->area_id = $request->area_id;
            // $customer->save();

            $order = new Order();
            $order->invoice_number = $this->getNextOrderNumber();
            $order->customer_id = Auth ::user () -> userable_id;
            $order->shipping_address_id = $request-> shipping_address_id;
            $order->status_id = 1;
            $deliveryCharge = $this->getDeliveryAmount( $request->delivery_type);
            $order->delivery_charge = $deliveryCharge;
            $order->setting_delivery_id =$request->delivery_type;
            $order->created_by = Auth ::user () -> userable_id;
            $order->updated_by = Auth ::user () -> userable_id;
            $order->save();

            $totalProductSummation = $this->createOrderDetailsInfo($order->id);
            $customerTotalDiscount = $this->customerTotalDiscount($order->id,$totalProductSummation);
            $order->discount =  $customerTotalDiscount ;
            $order->sub_total = $totalProductSummation;
            $order->grand_total = ($totalProductSummation + $deliveryCharge) - $customerTotalDiscount ;
            $order->save();
            $data['order'] = $order;
            DB::commit();
            return $this->respondCreated('Order Successfully Created', $data);

        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Recive order id insert cart data in order details table for tract individual item 
     * createOrderDetailsInfo() create individual item react in order_details table with item summation 
     * this method return total prodcut summation 
     * @param $orderId
     *
     * @author IAR
     */
    public function createOrderDetailsInfo($orderId)
    {
        $cartWithOtherInfo = Cart::whereUserId( Auth ::user () -> userable_id)->with('bookVariant')->get()->toArray();
        $orderDetailsData = [];
        $totalProductSummation = 0;
        foreach ($cartWithOtherInfo as $key => $value) {

            $orderDetailsData[$key]['order_id'] = $orderId;
            $orderDetailsData[$key]['book_variant_id'] = $value['variant_id'];
            $orderDetailsData[$key]['price'] = $value['book_variant']['price'] - $value['book_variant']['discount_value'];
            $orderDetailsData[$key]['qty'] = $value['quantity'];
            $orderDetailsData[$key]['total'] = $orderDetailsData[$key]['price'] * $value['quantity'];
            $totalProductSummation += $orderDetailsData[$key]['total'] ;
        }
        OrderDetails::insert($orderDetailsData);

        return  $totalProductSummation??0 ;
    }

    /**
     * this method return a unique invoice number for order 
     * @author IAR
     */
    public function getNextOrderNumber()
    {
        $lastOrder = Order::orderBy('created_at', 'desc')->first();
        if ( ! $lastOrder )
            $number = 0;
        else 
            $number = substr($lastOrder->order_no, 6);
        return date('Ym').sprintf('%06d', intval($number) + 1);
    }

    /**
     * this method current order delivery amount 
     * @param $deliveryTypeId
     * @author IAR
     */

    public function getDeliveryAmount($deliveryTypeId)
    {   
        $findDeliveryInfo = Delivery::find($deliveryTypeId);

        return $findDeliveryInfo->amount ;

    }

    /**
     * this method  return customer total discount for this order
     * This method also create order discount record for tracking every order discoutn 
     * @param $orderId
     * @param $totalSum 
     * @author IAR
     */
    public function customerTotalDiscount( $orderId ,  $totalSum)
    {
        $fixedOccationAmount = 0 ;
        $occasionDiscount = Discount::select('id','title','discount_type','value','status')
					->whereStatus(1)
					->where('started_at', '<', date('Y-m-d h:i:s'))
					->Where('ended_at', '>', date('Y-m-d h:i:s'))
					->first();
        if(isset($occasionDiscount)){

            $fixedOccationAmount = ( $totalSum * $occasionDiscount->value??0 ) / 100 ;
            $insertData =[
                'order_id'=>$orderId,
                'discount_id'=>$occasionDiscount->id,
                'type'=>'Occation Discount',
                'amount_fixed'=>$fixedOccationAmount,
                'amount_parcentage'=> $occasionDiscount->value
            ];
            OrderDiscount::create($insertData);
        }
        
        $fixedCustomerAmount  = 0 ;
        $customerDiscount = CustomerDiscount::where('customer_id',Auth ::user () -> userable_id)->select('value')->first();
        if(isset($customerDiscount)){
            $fixedCustomerAmount = ( $totalSum * $customerDiscount->value??0 ) / 100 ;
            $insertData =[
                'order_id'=>$orderId,
                'type'=>'Customer Discount',
                'amount_fixed'=>$fixedCustomerAmount,
                'amount_parcentage'=> $customerDiscount->value
            ];
            OrderDiscount::create($insertData);
        }
        
        
        return $fixedCustomerAmount + $fixedOccationAmount ;
    
    }

    public function saveOfflineSales(Request $request){
        DB::beginTransaction();
        try {
            $currentUser = Auth::user();
            $order = new Order();
            $order->invoice_number = rand(100000,999999);
            $order->customer_id = $request->customer_id;
            if($request->discount > 0){
                $order->discount = $request->discount;
            }
            
            $order->status_id = 1;
            $order->created_by = $currentUser->id;
            $order->updated_by = $currentUser->id;
            $order->save();
            if(count($request->itemList)){
                foreach($request->itemList as $item){
                    $bookVariant = BookVariant::findOrFail($item['varinatId']);
                    $orderDetails = new OrderDetails();
                    $orderDetails->order_id = $order->id;
                    $orderDetails->book_variant_id = $bookVariant->id;
                    $orderDetails->price = $bookVariant->price;
                    $orderDetails->qty = $item['qty'];
                    $orderDetails->save();
                }
            }
            DB::commit();
            return $this->respondCreated('Order Successfully Created');
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
        
    }



    public function returnOrders(){
        try {
            $data = [];
            $data['returnOrders'] = OrderReturn::with(['order','user.userable'])->get();
            return Response::json($data, Res::HTTP_OK);
        } catch (\Throwable $th) {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
        
    }
}