<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Res;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Common\SettingSlider;
use App\Models\Category;
use Response;

class CommonController extends Controller
{

    public function loadSliders()
    {
        $data = [];
        $data['sliders'] = SettingSlider::all();
        return Response::json($data,Res::HTTP_OK);
    }

    public function loadBookCategories()
    {
        $data = [];
        $data['book_categories'] = Category::whereStatus('active')->get();
        return Response::json($data,Res::HTTP_OK);
    }


}