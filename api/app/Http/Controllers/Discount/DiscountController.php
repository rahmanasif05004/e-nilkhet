<?php

namespace App\Http\Controllers\Discount;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Traits\ResponseJsonAble;
use App\Models\Discount\Discount;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response as Res;
use App\Http\Requests\DiscountRequest;
use Illuminate\Support\Facades\Response;
use App\Models\Discount\CustomerDiscount;
use App\Models\Delivery;
use App\Models\Cart;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DiscountController extends Controller
{
	use ResponseJsonAble;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    $data = [];
	    $data['discounts'] = Discount::with(['books','discountable'])->get();
	    return $this->respondCreated('Discount List',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	    //
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(DiscountRequest $request)
	{
	    try {
	        $discount = new Discount();
	        $discountFillableData = $request->only($discount->getFillable());
	        $discountFillableData['started_at'] = date('Y-m-d h:i:s', strtotime($request->input('started_at')));
            $discountFillableData['ended_at'] = date('Y-m-d h:i:s', strtotime($request->input('ended_at')));
            $discountFillableData['created_by'] = 1;

            if($request->input('discountable_type') == 'package'){
            	$discountFillableData['discountable_id'] = $request->input('packageId');
            }elseif($request->input('discountable_type') == 'customer_group'){
            	$discountFillableData['discountable_id'] = $request->input('customerGroupId');
            }else{
            	$discountFillableData['discountable_id'] = null;
            }

	        $discount->fill($discountFillableData)->save();

	        if($request->input('discountable_type') == 'item'){
	        	$discount->books()->sync($request->input('booksIds'));
	        }
	        
	        return $this->respondCreated('Discount Successfully Created');
	    } catch (Exception $e) {
	        DB::rollback();
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    try
	    {
	    	$data['discount'] = Discount::with(['books','discountable'])->findOrFail($id);
	        return $this->respondCreated('Discount Successfully Get',$data);
	    }
	    catch(ModelNotFoundException $e)
	    {
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
	    //
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(DiscountRequest $request, $id)
	{
	    try {
	        $discount = Discount::findOrFail($id);
	        $discountFillableData = $request->only($discount->getFillable());
	        $discountFillableData['started_at'] = date('Y-m-d h:i:s', strtotime($request->input('started_at')));
            $discountFillableData['ended_at'] = date('Y-m-d h:i:s', strtotime($request->input('ended_at')));

	        $discount->fill($discountFillableData)->save();


	        return $this->respondCreated('Discount Successfully Updated');
	    } catch (Exception $e) {
	        DB::rollback();
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    DB::beginTransaction();
	    try {
	        $package = Package::findOrFail($id);
	        if($package->discount){
	            $package->discount->delete();
	        }
	        $package->books()->detach();
	        $package->delete();
	        DB::commit();
	        return $this->respondCreated('Package Successfully Deleted');
	    } catch (Exception $e) {
	        DB::rollback();
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	    
	}

	public function activeDiscount(Request $request){
		try {
			$occasionDiscount = Discount::select('id','title','discount_type','value')
					->whereStatus(1)
					->where('started_at', '<', date('Y-m-d h:i:s'))
					->Where('ended_at', '>', date('Y-m-d h:i:s'))
					->first();
			
			$currentUser = Auth::user();
			$customerDiscount = CustomerDiscount::where('customer_id',$currentUser->userable_id)->select('value')->first();
		
			$data['occasionDiscount'] = $occasionDiscount??0;
			$data['customerDiscount'] = $customerDiscount->value??0;
			$data['deliveryInfo'] = Delivery::select('name_en','name_bn','amount','id')->where('status', 1)->get() ;

			return $this->respondCreated('Order  Discount',$data);

		} catch (\Throwable $th) {
			return $th;
			return $this->respondInternalError('Sorry, Operation Failed');
		}
		
		
	}

}
