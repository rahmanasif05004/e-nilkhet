<?php

namespace App\Http\Controllers\Discount;

use Illuminate\Http\Request;
use App\Models\Users\Customer;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Discount\CustomerDiscount;

class CustomerDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
	    $data['customerDiscounts'] = CustomerDiscount::with(['customer'])->get();
	    return $this->respondCreated('Customer Discount List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            CustomerDiscount::truncate();
            $customerDiscount = new CustomerDiscount();
            $inserData = [];
            $itemList = $request->all();
            foreach($itemList as $key => $item){
                $inserData[$key] = collect($item)->only($customerDiscount->getFillable())->toArray();
            }
            $customerDiscount->insert($inserData);

            DB::commit();
            return $this->respondCreated('Customer Discount Successfully Created');
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
