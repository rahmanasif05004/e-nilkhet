<?php

namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as Res;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\SettingSliderRequest;
use App\Models\SettingSlider;

class SettingSliderController extends Controller
{
    /**
     * @var variant
     */
    public $slider;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SettingSlider $slider)
    {
        $this->slider = $slider ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['sliders'] =$this->slider->all();
        return Response::json($data, Res::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingSliderRequest $request)
    {
        try{
            $data['slider'] = $this->slider->create($request->only($this->slider->getModel()->getFillable()));


            return $this->respondCreated('Slider Successfully Created',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['slider'] = $this->slider->findOrFail($id);
            return $this->respondCreated('Slider Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['slider'] = $this->slider->find($id);
           return $this->respondCreated('Slider Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingSliderRequest $request, $id)
    {
        try
        {
            $settingSlider  = $this->slider->find($id);
            $data['slider'] = $settingSlider->update($request->only($this->slider->getModel()->getFillable()));
            return $this->respondCreated('Slider Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $sliderInfo = $this->slider->findOrFail($id);
            if($sliderInfo->delete()){
                return $this->respondCreated('Slider Successfully Deleted');
            }else{
                return $this->respondNotFound('Slider Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Slider Not Deleted');
        }
    }
}