<?php

namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Res;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\SettingAreaRequest;
use App\Models\SettingArea;

class SettingAreaController extends Controller
{

    
    /**
     * @var variant
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SettingArea $area)
    {
        $this->model = $area ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $areas =$this->model->query()->with('district');
        if($request->input('district_id')){
            $areas->where('district_id', $request->input('district_id'));
        }
        $data['areas'] = $areas->get();
        return $this->respondCreated('Area  List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingAreaRequest $request)
    {
        try{
            
            $data['area']=$this->model->create($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Area Successfully Created',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['area'] = $this->model->findOrFail($id);
            return $this->respondCreated('Area Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['area'] = $this->model->find($id);
           return $this->respondCreated('Area Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingAreaRequest $request, $id)
    {

        
        try
        {
            $settingArea  = $this->model->find($id);
            $data['area'] = $settingArea->update($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Area Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $areaInfo = $this->model->findOrFail($id);
            if($areaInfo->delete()){
                return $this->respondCreated('Area Successfully Deleted');
            }else{
                return $this->respondNotFound('Area Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Area Not Deleted');
        }
    }
}