<?php

namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Res;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\SettingRequest;
use App\Models\SettingEditor;

class SettingEditorController extends Controller
{

   
    /**
     * @var Editor
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SettingEditor $editor)
    {
        $this->model = $editor;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['editor'] =$this->model->all();
        return $this->respondCreated('Editor  List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingRequest $request)
    {
        try{
            
            $data['editor']=$this->model->create($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Editor Successfully Created',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['editor'] = $this->model->findOrFail($id);
            return $this->respondCreated('Editor Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['editor'] = $this->model->find($id);
           return $this->respondCreated('Editor Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request, $id)
    {

        
        try
        {
            $findData  = $this->model->find($id);
            $data['editor'] = $findData->update($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Editor Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $findData = $this->model->findOrFail($id);
            if($findData->delete()){
                return $this->respondCreated('Editor Successfully Deleted');
            }else{
                return $this->respondNotFound('Editor Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Editor Not Deleted');
        }
    }
}