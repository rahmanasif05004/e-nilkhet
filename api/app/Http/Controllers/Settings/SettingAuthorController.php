<?php

namespace App\Http\Controllers\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\FileUpload;
use Illuminate\Http\Response as Res;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;
use App\Http\Requests\SettingRequest;
use App\Models\SettingAuthor;

class SettingAuthorController extends Controller
{
    use FileUpload;
    
    /**
     * @var Author
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SettingAuthor $author)
    {
        $this->model = $author;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['author'] =$this->model->all();
        return $this->respondCreated('Author  List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingRequest $request)
    {
        try{
            
            $data['author']=$this->model->create($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Author Successfully Created',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['author'] = $this->model->findOrFail($id);
            return $this->respondCreated('Author Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['author'] = $this->model->find($id);
           return $this->respondCreated('Author Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request, $id)
    {

        
        try
        {
            $findData  = $this->model->find($id);
            $data['author'] = $findData->update($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Author Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $findData = $this->model->findOrFail($id);
            $image_path = "/uploads/authors/".$findData->image;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            if($findData->delete()){
                return $this->respondCreated('Author Successfully Deleted');
            }else{
                return $this->respondNotFound('Author Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Author Not Deleted');
        }
    }


    public function imageUpload(Request $request){
        try 
        {
            $file = $request->file('file');
            $image =  $this->saveFormationSingleImage($file, 'authors');
            return $this->respondCreated('Author Image Uploaded', $image);
        } 
        catch (Exception $e) 
        {
            return $this->respondNotFound('Sorry Operation Failed');
        }
    }

    public function removeImage($id){
        try
        {
            $findData = $this->model->findOrFail($id);
            $image_path = "/uploads/authors/".$findData->image;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            return $this->respondCreated('Image Successfully Deleted');
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Author Image Not Deleted');
        }
    }
}