<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Res;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\SettingZoneRequest;
use App\Models\SettingZone;


class SettingZoneController extends Controller
{

   
    /**
     * @var Zone
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SettingZone $zone)
    {
        $this->model = $zone;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        $data = [];
        $zones =$this->model->query()->with('area');
        if($request->input('area_id')){
            $zones->where('setting_area_id', $request->input('area_id'));
        }
        $data['zones'] = $zones->get();
        return $this->respondCreated('Zone  List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingZoneRequest $request)
    {
        try{
            
            $data['zone']=$this->model->create($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Zone Successfully Created',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['zone'] = $this->model->findOrFail($id);
            return $this->respondCreated('Zone Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['zone'] = $this->model->find($id);
           return $this->respondCreated('Zone Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingZoneRequest $request, $id)
    {

        
        try
        {
            $findData  = $this->model->find($id);
            $data['zone'] = $findData->update($request->only($this->model->getModel()->getFillable()));
            return $this->respondCreated('Zone Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $findData = $this->model->findOrFail($id);
            if($findData->delete()){
                return $this->respondCreated('Zone Successfully Deleted');
            }else{
                return $this->respondNotFound('Zone Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Zone Not Deleted');
        }
    }
}
