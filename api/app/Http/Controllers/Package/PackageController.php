<?php

namespace App\Http\Controllers\Package;

use DB;
use Auth;
use App\Http\Requests\PackageRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\FileUpload;
use App\Models\Discount\Package;
use App\Models\Discount\Discount;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as Res;
use App\Traits\ResponseJsonAble;

class PackageController extends Controller
{
	use ResponseJsonAble, FileUpload;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    $data = [];
	    $data['packages'] = Package::withCount('books')->get();
	    return $this->respondCreated('Package List',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	    //
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(PackageRequest $request)
	{
	    DB::beginTransaction();
	    try {
	        $package = new Package();
	        $packageFillable = $request->only($package->getFillable());
	        $package->fill($packageFillable)->save();

	        $package->books()->sync($request->input('booksIds'));
	        
	        DB::commit();
	        return $this->respondCreated('Package Successfully Created');
	    } catch (Exception $e) {
	        DB::rollback();
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    try
	    {
	        $data['packages'] = Package::with(['books','discount'])->findOrFail($id);
	        return $this->respondCreated('Package Successfully Get',$data);
	    }
	    catch(ModelNotFoundException $e)
	    {
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
	    //
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(PackageRequest $request, $id)
	{
	    DB::beginTransaction();
	    try {
	        $package = Package::findOrFail($id);
	        $packageFillable = $request->only($package->getFillable());
	        $package->fill($packageFillable)->save();

	        $package->books()->sync($request->input('booksIds'));
	        
	        DB::commit();
	        return $this->respondCreated('Package Successfully Updated');
	    } catch (Exception $e) {
	        DB::rollback();
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    DB::beginTransaction();
	    try {
	        $package = Package::findOrFail($id);
	        if($package->discount){
	            $package->discount->delete();
	        }
	        $package->books()->detach();
	        $package->delete();
	        DB::commit();
	        return $this->respondCreated('Package Successfully Deleted');
	    } catch (Exception $e) {
	        DB::rollback();
	        return $this->respondInternalError('Sorry, Operation Failed');
	    }
	    
	}

	public function imageUpload(Request $request)
    {
        try 
        {
            $file = $request->file('file');
            $image =  $this->saveFormationSingleImage($file, 'packages');
            return $this->respondCreated('Package Image Uploaded', $image);
        } 
        catch (Exception $e) 
        {
            return $this->respondNotFound('Package Image Not Uploaded');
        }
    }

}
