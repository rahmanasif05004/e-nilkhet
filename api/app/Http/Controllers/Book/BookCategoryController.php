<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Res;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Category;
use DB;
class BookCategoryController extends Controller
{

    
    /**
     * @var variant
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->model = $category ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['collections'] =$this->model->with('products')->get();
        return $this->respondCreated('Book Categsory  List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        // DB::beginTransaction();
        // try{
        //     $fillableData = $request->only($this->model->getModel()->getFillable());
        //     $fillableData['created_by'] = 1;
        //     $fillableData['updated_by'] = 1;
        //     $data['book'] = $this->model->create($fillableData);
        //     if($request->variant){
        //         foreach ($request->variant as $key => $value) {
        //             $bookVariant = new BookVariant();
        //             $bookVariantFillableData = collect($value)->only($bookVariant->getFillable())->toArray();
        //             $bookVariantFillableData['book_id'] = $data['book']->id;
        //             $bookVariantFillableData['reorder_qty'] = $bookVariantFillableData['stock_qty'];
        //             $bookVariant->fill($bookVariantFillableData)->save();
        //         }
        //     }
        //     DB::commit();
        //     return $this->respondCreated('Book Successfully Created',$data);
        // }catch(ModelNotFoundException $e){
        //     DB::rollBack();
        //     return $this->respondInternalError('Sorry, Operation Failed');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // try
        // {
        //     $data['book'] = $this->model->with('variants')->findOrFail($id);
        //     return $this->respondCreated('Book Successfully Get',$data);
        // }
        // catch(ModelNotFoundException $e)
        // {
        //     return $this->respondInternalError('Sorry, Operation Failed');
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // try
        // {
        //    $data['book'] = $this->model->with('variants')->findOrFail($id);
        //    return $this->respondCreated('Book Successfully Get',$data);
        // }
        // catch(ModelNotFoundException $e)
        // {
        //     return $this->respondInternalError('Sorry, Operation Failed');
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {

        
        // try
        // {
        //     $Book  = $this->model->with('variants')->findOrFail($id);
        //     $data['book'] = $Book->update($request->only($this->model->getModel()->getFillable()));
        //     return $this->respondCreated('Book Successfully Updated',$data);
        // }
        // catch(ModelNotFoundException $e)
        // {
        //     return $this->respondInternalError('Sorry, Operation Failed');
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // try
        // {
        //     $bookInfo = $this->model->with('variants')->findOrFail($id);
        //     if($bookInfo->delete()){
        //         return $this->respondCreated('Book Successfully Deleted');
        //     }else{
        //         return $this->respondNotFound('Book Not Deleted');
        //     }
        // }
        // catch(ModelNotFoundException $e)
        // {
        //     return $this->respondNotFound('Book Not Deleted');
        // }
    }
}
