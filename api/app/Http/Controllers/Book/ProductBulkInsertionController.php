<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\FileUpload;


class ProductBulkInsertionController extends Controller
{
    use FileUpload;
    public function csvFileUpload(Request $request)
    {
        try 
        {
            $path = request()->file('file')->getRealPath();
            $file = file($path);//turn into array
            $data = array_slice($file, 1);//to remove first row
            $chunks=array_chunk($data,1000);
            foreach($chunks as $key=>$chunkedCsvContant)
            {
                $chunkedFileName=public_path('uploads/csvs/'.date('d-m-Y').'_'.$key.'.csv');
                file_put_contents($chunkedFileName,$chunkedCsvContant);
            }
            unlink($path);
            return $this->respondCreated('CSV uploaded', $chunks);
        } 
        catch (Exception $e) 
        {
            return $this->respondNotFound('Book Image Not Uploaded');
        }
    }

    private function parseChunkedCsvAndInsert()
    {

    }
}
