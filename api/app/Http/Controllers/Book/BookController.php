<?php

namespace App\Http\Controllers\Book;
use DB;
use App\Models\Book;
use App\Traits\FileUpload;
use App\Models\BookVariant;
use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response as Res;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BookController extends Controller
{
    use FileUpload;
    
    /**
     * @var variant
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Book $book)
    {
        $this->model = $book ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['books'] =$this->model->with('variants')->get();
        return $this->respondCreated('Book  List',$data);
    }

    /**
     * Display Product Details with all realationship 
     *
     * @return \Illuminate\Http\Response
     */
    public function getBookInfo($bookSlug)
    {   
        $data = [];
        $data['book'] =$this->model->whereSlug($bookSlug)->with('variants.variantInfo','country','author','publisher','translator','editor','ratings.user')->first();
        return $this->respondCreated('Book  List',$data);
    }

    /**
     * Display Product Details with all realationship
     *
     * @return \Illuminate\Http\Response
     */
    public function getBookInfoByWishList(Request $request)
    {   
        $data = [];
        foreach($request->productList as $list)
        {
             $data[] = $this->model->whereId($list)->with('author','publisher')->first();
        }
        return $this->respondCreated('Wishlist Data List',$data);
    }

    




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        DB::beginTransaction();
        try{
            $book = new Book();
            $fillableData = $request->only($this->model->getModel()->getFillable());
            // $fillableData['slug'] = strtolower(str_replace(' ','-',$request->name_en));
            $fillableData['created_by'] = 1;
            $fillableData['updated_by'] = 1;
            $book->fill($fillableData)->save();
            $book->categories()->attach($request->categoriesIds);
            $variants=$request->variants;
            $variants_to_insert=[];
            foreach ($variants as $key => $value) {
                $variants_to_insert[$key]['active_status']=$value['active_status'];
                $variants_to_insert[$key]['discount_type']=$value['discount_type'];
                $variants_to_insert[$key]['discount_value']=$value['discount_value'];
                $variants_to_insert[$key]['number_of_page']=$value['number_of_page'];
                $variants_to_insert[$key]['price']=$value['price'];
                $variants_to_insert[$key]['reorder_qty']=$value['reorder_qty'];
                $variants_to_insert[$key]['stock_qty']=$value['stock_qty'];
                $variants_to_insert[$key]['variant_id']=$value['variant_id'];
                $variants_to_insert[$key]['book_id']=$book->id;
            }
            $book->variants()->insert($variants_to_insert);
            $data['book'] = $book;
            DB::commit();
            return $this->respondCreated('Book Successfully Created',$data);
        }catch(ModelNotFoundException $e){
            DB::rollBack();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['book'] = $this->model->with('variants')->findOrFail($id);
            return $this->respondCreated('Book Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['book'] = $this->model->with('variants','categories')->findOrFail($id);
           return $this->respondCreated('Book Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {

        try
        {
            $book  = $this->model->with('variants')->findOrFail($id);
            $data['book'] = $book->update($request->only($this->model->getModel()->getFillable()));
            $book->categories()->sync($request->categoriesIds);
            $variants=$request->variants;
            $saved_variant_row_ids=array_column($book->variants->toArray(),'id');
            $updated_variant_row_ids=array_column($variants,'id');
            $variant_rows_to_remove=array_diff($saved_variant_row_ids,$updated_variant_row_ids);
            $book->variants()->whereIn('id',$variant_rows_to_remove)->delete();
            $variants_to_update=[];
            foreach ($variants as $key => $value) {
                BookVariant::updateOrCreate([
                    'id'=>$value['id']??null
                ],
                [
                    'active_status' =>$value['active_status'],
                    'discount_type' =>$value['discount_type'],
                    'discount_value'=>$value['discount_value'],
                    'number_of_page'=>$value['number_of_page'],
                    'price'         =>$value['price'],
                    'reorder_qty'   =>$value['reorder_qty'],
                    'stock_qty'     =>$value['stock_qty'],
                    'variant_id'    =>$value['variant_id'],
                    'book_id'       =>$book->id,
                ]);
                
            }
            return $this->respondCreated('Book Successfully Updated',$variant_rows_to_remove);
        }
        catch(\Exception $e)
        {
            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $bookInfo = $this->model->with('variants')->findOrFail($id);
            if($bookInfo->delete()){
                return $this->respondCreated('Book Successfully Deleted');
            }else{
                return $this->respondNotFound('Book Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Book Not Deleted');
        }
    }

    public function imageUpload(Request $request)
    {
        try 
        {
            $file = $request->file('file');
            $image =  $this->saveFormationSingleImage($file, 'books');
            return $this->respondCreated('Book Image Uploaded', $image);
        } 
        catch (Exception $e) 
        {
            return $this->respondNotFound('Book Image Not Uploaded');
        }
    }

    public function getBookVariants(Request $request){
        try {
            $bookVariants = BookVariant::with(['book','variantInfo']);
            if($request->input('title')){
                $title = $request->input('title');
                $bookVariants->whereHas('book', function($query) use ($title){
                    $query->where('title_en', 'like','%'.$title.'%');
                    $query->orwhere('title_bn', 'like','%'.$title.'%');
                });
            }
            if($request->input('author_id')){
                $authorId = $request->input('author_id');
                $bookVariants->whereHas('book', function($query) use ($authorId){
                    $query->whereAuthorId($authorId);
                });
            }
            if($request->input('publisher_id')){
                $publisherId = $request->input('publisher_id');
                $bookVariants->whereHas('book', function($query) use ($publisherId){
                    $query->wherePublisherId($publisherId);
                });
            }
            if($request->input('editor_id')){
                $editorId = $request->input('editor_id');
                $bookVariants->whereHas('book', function($query) use ($editorId){
                    $query->whereEditorId($editorId);
                });
            }

            if($request->input('except')){
                $except = $request->input('except');
                if(is_array($except)){
                    $bookVariants->whereNotIn('id', $except);
                }else{
                    $bookVariants->where('id', '!=', $except);
                }
            }
            
            $data['bookVariants'] = $bookVariants->get();
            return $this->respondCreated('Book  List',$data);
        } catch (\Throwable $th) {
            // return $th;
            return $this->respondNotFound('Sorry!!, opetation failed');
        }
    }

    public function saveBookVariantsDiscount(Request $request){
     
        $validator = Validator::make($request->all(), 
            [ 
                'checkItems' => 'required|array|min:1', 
                'discount_type' => 'required', 
                'discount_value' => 'required', 
            ]);

        if ($validator->fails()) { 
            $message = $validator->errors(); 
            return $this->respondValidationError('Please filled Discount type and value', $message);
        }
        $discount_type = $request->input('discount_type');
        $discount_value = $request->input('discount_value');
        DB::beginTransaction();
        try {
            $items = $request->input('checkItems');
            foreach($items as $variantId){
                $bookVariant = BookVariant::findOrFail($variantId);
                $bookVariant->discount_type = $discount_type;
                $bookVariant->discount_value = $discount_value;
                $bookVariant->save();
            }

            DB::commit();
            return $this->respondCreated('Discount Created');
        } catch (\Throwable $th) {
            DB::rollback();
            return $this->respondNotFound('Sorry!!, opetation failed');
        }

        
    }
}
