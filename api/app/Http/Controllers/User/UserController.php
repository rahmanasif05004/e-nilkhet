<?php

namespace App\Http\Controllers\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponseJsonAble;
use App\Http\Requests\User\UserRequest;
use App\Models\Users\User;
use DB;

class UserController extends Controller
{
	use ResponseJsonAble;
    /**
     * @var variant
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->model = $user ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
    	DB::beginTransaction();
    	try 
    	{
    		$user  = $this->model->with('userable')->findOrFail($id);
	    	$data['user'] = $user->update($request->only($this->model->getModel()->getFillable()));
	    	$userableFillData = collect($request->input('userable'))->only($user->userable->getFillable())->toArray();
	    	// return $userableFillData;
	    	$user->userable->fill($userableFillData)->save();

	    	DB::commit();
            return $this->respondCreated('Order Successfully Created', $user);

    	} 
    	catch (ModelNotFoundException $e) 
    	{
    		DB::rollback();
            return $this->respondInternalError('Sorry, Operation Failed');
    	}
    	

    }
}
