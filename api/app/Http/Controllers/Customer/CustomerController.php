<?php

namespace App\Http\Controllers\Customer;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as Res;
use App\Traits\ResponseJsonAble;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\User\UserRequest;
use App\Models\Users\User;
use App\Models\Users\Customer;

class CustomerController extends Controller
{

    use ResponseJsonAble;
    /**
     * @var variant
     */
    // public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() // Customer $customer
    {
        // $this->model = $customer ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $customers = Customer::with('user');
        if($request->input('query')){
            $query = $request->input('query');
            $customers->where(function($parentQuery) use ($query){
                $parentQuery->where('name', 'LIKE', '%'.$query.'%');
                $parentQuery->orWhereHas('user', function($q) use ($query){
                    $q->where('phone', 'LIKE', '%'.$query.'%');
                    $q->orWhere('email', 'LIKE', '%'.$query.'%');
                });
            });
            
        }
        if($request->input('except')){
            $except = $request->input('except');
            if(is_array($except)){
                $customers->whereNotIn('id', $except);
            }else{
                $customers->where('id', '!=', $except);
            }
        }
        $data['customers'] = $customers->get();
        return Response::json($data, Res::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return Response::json($data,Res::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        DB::beginTransaction();
        try{
            // customer
            $customerModel = new Customer;
            $customerRequestData = $request->only($customerModel->getFillable());
            $customerId = $customerModel->insertGetId($customerRequestData);
            // user
            $userModel = new User;
            $userRequestData = collect($request->only(['phone','email','password']));
            $userRequestData = $userRequestData->merge(['userable_type'=> 'customer','userable_id'=> $customerId]);
            $userModel->insert($userRequestData->toArray());
            DB::commit();
            return $this->respondCreated('Customer Successfully Created');
        }
        catch(ModelNotFoundException $e)
        {
            DB::rollBack();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['customers'] = Customer::with('user')->findOrFail($id);
            return $this->respondCreated('Customer successfully get', $data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $data['customers'] = Customer::with('user')->findOrFail($id);
           return $this->respondCreated('Customer Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $customer  = Customer::findOrFail($id);
            $data['customer'] = $customer->update($request->only($customer->getFillable()));
            return $this->respondCreated('Customer Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $customer = Customer::findOrFail($id);
            if($customer->delete()){
                return $this->respondCreated('Customer Successfully Deleted');
            }else{
                return $this->respondNotFound('Customer Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Something went wrong!');
        }
    }

    public function getCustomer()
    {
        try
        {
            $data['customer'] = $this->model->with('orders')->findOrFail(1);
            return $this->respondCreated('Customer Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }    
    }


    public function saveCustomer(Request $request){
        try{
            // customer
            $customerModel = new Customer;
            $customerRequestData = $request->only($customerModel->getFillable());
            $customerModel->fill($customerRequestData)->save();
            $data['customer'] = $customerModel->toArray();
            return $this->respondCreated('Customer Successfully Created', $data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

}
