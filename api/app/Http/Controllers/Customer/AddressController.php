<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Common\Address;
use  App\Http\Requests\Customer\AddressRequest;
use Auth;

class AddressController extends Controller
{
    /**
     * @var Zone
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Address $address)
    {
        $this->model = $address;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $data['addresses'] = $this->model->with('district','area','zone')->where('user_id', Auth::user()->id)->get();
        return $this->respondCreated('Address  List',$data);
    }

    public function store(AddressRequest $request){

        try{
            $fillableData = $request->only($this->model->getModel()->getFillable());
            $fillableData['user_id'] = Auth::user()->id;
            $data['address'] = $this->model->create($fillableData);
            $data['addresses'] = $this->model->with('district','area','zone')->where('user_id', Auth::user()->id)->get();
            return $this->respondCreated('Zone Successfully Created',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddressRequest $request, $id)
    {
        try
        {
            $findData  = $this->model->find($id);
            $data['address'] = $findData->update($request->only($this->model->getModel()->getFillable()));
            $data['addresses'] = $this->model->with('district','area','zone')->where('user_id', Auth::user()->id)->get();
            return $this->respondCreated('Address Successfully Updated',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $findData = $this->model->findOrFail($id);
            if($findData->delete()){
                return $this->respondCreated('Address Successfully Deleted');
            }else{
                return $this->respondNotFound('Address Not Deleted');
            }
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondNotFound('Address Not Deleted');
        }
    }
}
