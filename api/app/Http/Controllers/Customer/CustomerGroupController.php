<?php

namespace App\Http\Controllers\Customer;

use DB;
use Auth;
use App\Http\Requests\Customer\CustomerGroupRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\CustomerGroup;
use App\Models\Discount\Discount;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as Res;
use App\Traits\ResponseJsonAble;

class CustomerGroupController extends Controller
{
    use ResponseJsonAble;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['customerGroups'] = CustomerGroup::withCount('customers')->get();
        return $this->respondCreated('Customer Group List',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerGroupRequest $request)
    {
        DB::beginTransaction();
        try {
            $customerGroup = new CustomerGroup();
            $customerGroupFillable = $request->only($customerGroup->getFillable());
            $customerGroup->fill($customerGroupFillable)->save();

            $customerGroup->customers()->sync($request->input('customerIds'));

            if($request->input('discount.value')){
                $discount = new Discount();
                $discountFillable = collect($request->input('discount'))->only($discount->getFillable())->toArray();
                $discountFillable['started_at'] = date('Y-m-d h:i:s', strtotime($request->input('discount.started_at')));
                $discountFillable['ended_at'] = date('Y-m-d h:i:s', strtotime($request->input('discount.ended_at')));
                $discountFillable['discountable_id'] = $customerGroup->id;
                $discountFillable['created_by'] = 1;
                $discount->fill($discountFillable)->save();
            }
            DB::commit();
            return $this->respondCreated('Customer Group Successfully Created');
        } catch (Exception $e) {
            DB::rollback();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data['customerGroups'] = CustomerGroup::with(['customers','discount'])->findOrFail($id);
            return $this->respondCreated('Customer Group Successfully Get',$data);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerGroupRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $customerGroup = CustomerGroup::findOrFail($id);
            $customerGroupFillable = $request->only($customerGroup->getFillable());
            $customerGroup->fill($customerGroupFillable)->save();

            $customerGroup->customers()->sync($request->input('customerIds'));

            if($request->input('discount.value')){
                $discount = $customerGroup->discount ?: new Discount();
                $discountFillable = collect($request->input('discount'))->only($discount->getFillable())->toArray();
                $discountFillable['discountable_id'] = $customerGroup->id;
                $discountFillable['created_by'] = 1;
                $discount->fill($discountFillable)->save();
            }
            DB::commit();
            return $this->respondCreated('Customer Group Successfully Updated');
        } catch (Exception $e) {
            DB::rollback();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $customerGroup = CustomerGroup::findOrFail($id);
            if($customerGroup->discount){
                $customerGroup->discount->delete();
            }
            $customerGroup->customers()->detach();
            $customerGroup->delete();
            DB::commit();
            return $this->respondCreated('Customer Group Successfully Deleted');
        } catch (Exception $e) {
            DB::rollback();
            return $this->respondInternalError('Sorry, Operation Failed');
        }
        
    }
}
