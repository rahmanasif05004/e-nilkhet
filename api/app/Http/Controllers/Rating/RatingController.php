<?php

namespace App\Http\Controllers\Rating;

use App\Http\Controllers\Controller;
use App\Http\Requests\RatingRequest;
use Illuminate\Http\Request;
use App\Models\Common\Rating;
use Auth;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $currentUser = Auth::user();
            $ratings = Rating::orderBy('id','desc')->where('user_id', $currentUser->id);
            if($request->input('book_id')){
                $ratings->where('book_id', $request->input('book_id'));
            }
            $ratings = $ratings->get();
            return $this->respondCreated('Ratings Data Found', $ratings);
        } catch (Exception $e) {
            return $this->respondInternalError('Sorry, Operation Failed');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RatingRequest $request)
    {
        try {
            $currentUser = Auth::user();
            $rating = new Rating();
            $ratingFillData = $request->only($rating->getFillable());
            if($currentUser->userable_type != 'admin'){
                $ratingFillData['user_id'] = $currentUser->id;
            }
            $ratingFillData['user_id'] = 1;
            $rating->fill($ratingFillData)->save();
            $rating->load('user');
            return $this->respondCreated('Page Successfully Created', $rating);
        } catch (Exception $e) {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
