<?php
namespace App\Http\Controllers\Authentication;
use DB;
use App\Http\Requests\User\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use Hash;
use App\Models\Users\Customer;

class RegistrationController extends Controller
{
    public function customerRegister(UserRequest $request)
    {
        try {
        	$customer = new Customer();
	        $customer->name = $request->customer['name'];
            $customer->profession_id = $request->customer['profession_id'];
	        $customer->save();
	        $customer->user()->create([
	            'phone'=>$request->phone,
	            'email'=>$request->email,
	            'username'=>$request->username,
	            'password'=> $request->password,
	            'profession_id'=> $request->profession_id,
	        ]);
            return $this->respondCreated('Customer Successfully Created',$customer);
        } catch (ModelNotFoundException $e) {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
        
    }
}
