<?php

namespace App\Http\Controllers\Authentication;
use DB;
use App\Http\Controllers\Controller;
use App\Models\Users\User;
use App\Models\Users\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\UserRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoginController extends Controller
{
    /**
     * This method is used to check if a customer is authenticated 
     * @author Md. Asif Rahman
     * @param $request Request
     * @return json
     */
    public function customerLogin(Request $request)
    {
        return $this->validateUserAndGenerateToken(User::CUSTOMER,$request);
    }


     /**
     * This method is used to check if an admin is authenticated 
     * @author Md. Asif Rahman
     * @param $request Request
     * @return json
     */
    public function adminLogin(Request $request)
    {
        return $this->validateUserAndGenerateToken(User::ADMIN,$request);
    }

    /**
     * This method is used to retrive currently 
     * loggedin user information
     */
    public function me()
    {
        $loggedInUserId=Auth::id();
        $user=User::find($loggedInUserId);
        return $this->respondFound("data found",$user);
    }

    public function logout()
    {
        $user = request()->user(); //or Auth::user()
        // Revoke current user token
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
    }

    /**
     * This method is used to authenticate any types of users
     * and generate access token
     */
    private function validateUserAndGenerateToken($userType,$request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::whereEmail($request->email)->whereUserableType($userType)
                ->whereIsActive(User::IS_ACTIVE)->first();

        if(!$user || !Hash::check($request->password,$user->password))
        {
            throw ValidationException::withMessages([
                'email'=>'The provided credentials are not correct'
            ]);
        }
        
        $token=$user->createToken($request->email)->plainTextToken;
        return $this->respondWithToken($token,$user);
    }
}
