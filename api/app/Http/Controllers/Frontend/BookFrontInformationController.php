<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Discount\Package;
use App\Models\Category;
use App\Models\BookCategory;
use App\Models\BookVariant;
class BookFrontInformationController extends Controller
{
    /**
     * @var variant
     */
    public $bookModel;
    public $category;
    public $bookVariant;
    public $bookCategory;
    public $packageModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Book $bookModel , Category $category , BookVariant $bookVariant , BookCategory $bookCategory , Package $packageModel )
    {
        $this->bookModel = $bookModel ;
        $this->category = $category ;
        $this->bookVariant = $bookVariant ;
        $this->bookCategory = $bookCategory ; 
        $this->packageModel = $packageModel ;
    }

    
    public function getPreOrderBook()
    {
         $collections = $this->bookModel->whereIsPreorder('YES')->get();
         if( $collections->count() > 0 ){
             return $this->respondFound('Pre Order Book List' , $collections );
         }else{
             return $this->respondFound('Pre Order Book List' , [] );
         }
    }

    /**
     * Get Parent Category Wise Product For Home Page Product Show 
     * Every Category Have some limit Product 10
     * @return \Illuminate\Http\Response
     */
    public function getParentCategoryWiseProduct ()
    {
        try {

            $collections = $this->category->whereParentId(0)->get()->map(
                function( $item ) {
                    $singleCategoryId = $this->category->whereParentId($item->id)->pluck('id');
                    $singleCategoryId[] = $item->id;
                    $bookIdList = $this->bookCategory->whereIn('category_id',$singleCategoryId)->pluck('book_id');
                    $item['books'] = $this->bookModel->whereIn('id',
                    $bookIdList)->whereIsPreorder('NO')->WithAndWhereHas(
                            'activeVariant' , function ( $query ) {
                            $query->where('active_status' , 'Yes' );
                        }
                        )->with('activeVariant.variantInfo')->take(6)->get();
                    return $item;

                }
            );
            return $this->respondCreated('Category Wise Book List' , $collections );

        }
        catch ( ModelNotFoundException $e ) {
            return $this->respondInternalError( 'Sorry, Operation Failed' );
        }
    }


    /**
     * Get Category wise Prodcut 
     * @return \Illuminate\Http\Response
     */
    public function getCategoryWiseBook ($categorySlug = null)
    {
        try {

            if ($categorySlug) {

                $findCategory  = $this->category->whereSlug ($categorySlug)->first();
                $bookIdList = $this->bookCategory->whereCategoryId($findCategory->id)->pluck('book_id');
                
                $collections = $this->bookModel->whereIn('id',$bookIdList)->whereIsPreorder('NO')->WithAndWhereHas('activeVariant', function ($query) {

                    $query->where('active_status', 'Yes');

                })->with('activeVariant.variantInfo')->get(); 

            } else {

                $collections = $this->bookModel->WithAndWhereHas('activeVariant', function ($query) {
                    $query->where('active_status', 'Yes');
                })->with('activeVariant.variantInfo')->get();
            }

            return $this->respondCreated('Collection  Data List', compact('collections'));

        } catch (ModelNotFoundException $e) {

            return $this->respondInternalError('Sorry, Operation Failed');

        }
    }

     /**
     * Get menu wise product 
     * menuName use for which type  data qury from table
     * menuId use for  query from  book aginast specific author , writter etc  
     * @return \Illuminate\Http\Response
     */
    public function getSubMenuWiseBook ($menuName ,$menuItemId )
    {
        try {

            $collectionsQuery = $this->bookModel->WithAndWhereHas('activeVariant', function ($query) {
                $query->where('active_status', 'Yes');
            });

            switch($menuName){

                case 'author':
                    $collectionsQuery = $collectionsQuery->whereAuthorId($menuItemId);
                break;
                // case 'subject':
                //     $collectionsQuery = $collectionsQuery->whereSubjectId($menuItemId);
                // break;
                case 'translator':
                    $collectionsQuery = $collectionsQuery->whereTranslatorId($menuItemId);
                break;
                case 'publisher':
                    $collectionsQuery = $collectionsQuery->wherePublisherId($menuItemId);
                break;
                // case 'languages':
                //     $collectionsQuery = $collectionsQuery->whereAuthorId($menuItemId);
                // break;
                case 'editor':
                    $collectionsQuery = $collectionsQuery->whereEditorId($menuItemId);
                break;

            }
            $collections = $collectionsQuery->with('activeVariant.variantInfo')->get();

            return  $collections;

        } catch (ModelNotFoundException $e) {

            return [];

        }
    }

    public function getProductDefaultShopProduct( $name = null , $id = null)
    {
        try {
            if($name && $id){
                $collections = $this->getSubMenuWiseBook($name,$id);
            }else{
                $collections = $this->bookModel->WithAndWhereHas('activeVariant', function ($query) {
                    $query->where('active_status', 'Yes');
                })->with('activeVariant.variantInfo')->get();
            }
            
            return $this->respondCreated('Collection  Data List', compact('collections'));

        } catch (ModelNotFoundException $e) {

            return $this->respondInternalError('Sorry, Operation Failed');

        }
    } 

    public function getPackageProductList()
    {
        $collections = $this->packageModel->whereStatus(1)->get();
         if( $collections->count() > 0 ){
             return $this->respondFound('Package List' , $collections );
         }else{
             return $this->respondFound('Package Book List' , [] );
         }
    }

    public function getPackageWiseBook ($packageSLug = null)
    {
         try {

            $collections = $this->packageModel->whereSlug($packageSLug)->with(['books' => function($query){
                $query->whereIsPreorder('NO');
                $query->whereHas('activeVariant', function($query){
                    $query->where('active_status', 'Yes');
                });
                $query->with('activeVariant');
            },'books.author','books.publisher'])->first();
            return $this->respondCreated('Collection  Data List', compact('collections'));

        } catch (ModelNotFoundException $e) {

            return $this->respondInternalError('Sorry, Operation Failed');

        }
    }

}
