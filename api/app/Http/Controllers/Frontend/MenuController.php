<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SettingAuthor;
use App\Models\SettingEditor;
use App\Models\SettingPublisher;
use App\Models\SettingTranslator;
class MenuController extends Controller
{

    /**
     * @var variant
     */
    public $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->model = $category ;
    }

    /**
     * Get Book Category List For Menu .
     * @param Array 
     */

     public function getBookMenuCategory()
     {
        try{
            
            $collections= $this->model->with('child')->whereParentId(0)->get();
            
            return $this->respondFound('Book menu Category  List', $collections);
        }
        catch(ModelNotFoundException $e)
        {
            return $this->respondInternalError('Sorry, Operation Failed');
        }
     }

     /**
     * Get all category And SUbcategory lsit 
     *
     * @return void
     */

    public function getStickyMenuCategory($categorySlug)
    {
        try {
            if($categorySlug){
                $findCategory  = $this->model->whereSlug ($categorySlug)->first();
                $categories =   $this->model->whereParentId($findCategory->id)->select('id','name_en','name_bn', 'slug','parent_id')->with('child')->get() ; 
                
                return $this->respondFound('Sticky Menu Category', $categories);
                
            }
            
            return $this->respondFound('Sticky Menu Category', []);

        } catch (ModelNotFoundException $e) {
           
            return $this->respondInternalError('Sorry, Operation Failed');
        }
        
    }

     /**
     * Get menu wise product 
     * menuName use for which type  data qury from table
     * @return \Illuminate\Http\Response
     */
    public function getSubMenuList ()
    {
        try {

            $data['author'] = SettingAuthor::whereStatus('Active')->select('id','name_bn','name_en')->get();
            $data['publisher'] = SettingPublisher::whereStatus('Active')->select('id','name_bn','name_en')->get();
            $data['editor'] = SettingEditor::whereStatus('Active')->select('id','name_bn','name_en')->get();
            $data['translator'] = SettingTranslator::whereStatus('Active')->select('id','name_bn','name_en')->get();
            return $this->respondCreated('Menu  Data List', compact('data'));

        } catch (ModelNotFoundException $e) {

            return $this->respondInternalError('Sorry, Operation Failed');

        }
    }

}
