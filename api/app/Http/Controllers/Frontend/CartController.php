<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use Auth;
use App\Models\BookVariant;
use App\Models\Cart;
Use App\Models\User;

class CartController extends Controller
{


    public function getAuthenticateUserCartData ()
    {
        try {
            $data = Cart ::whereUserId ( Auth ::user () -> userable_id ) -> select ('product_id' , 'variant_id' , 'quantity') -> get ();
            return $this -> respondCreated ( 'Cart  List' , $data );
        }
        catch ( ModelNotFoundException $e ) {
            return $this -> respondInternalError ( 'Sorry, Operation Failed' );
        }

    }

    public function checkCartProduct ( Request $request )
    {
        $cartItemInfo = $request -> cart;
        foreach ($cartItemInfo as $key => $item) {
            
            $existStatus = BookVariant ::whereBookId ( $item[ 'product_id' ] ) -> whereId (
                $item[ 'variant_id' ]
            ) -> first ();

            if ( !$existStatus ) {
                unset( $cartItemInfo[ $key ] );
            }
        }
        if ( Auth ::check () ) {
            $this -> storeCartInformation ( $cartItemInfo );
        }
        $data[ 'cart' ] = $cartItemInfo;
        $data[ 'products' ] = $this -> getCartItemForProduct ( $data[ 'cart' ] );
        return $this -> respondCreated ( 'Cart  List' , $data );
    }

    public function storeCartInformation ( $data )
    {
        //first delete all cart item in this user ;
        Cart ::whereUserId ( Auth ::user () -> userable_id ) -> delete ();
        //insert new multiple item in cart
        foreach ($data as $item) {
            Cart ::create ( $item );
        }
    }

    /**
     * Get Cart Item Producct .
     *
     * @param  Array
     * @param  intiger productId, VendorID
     */

    public function getCartItemForProduct ( $cart )
    {
        $data = [];

        foreach ($cart as $item) {

            $productId = $item[ 'product_id' ];
            $variantId = $item[ 'variant_id' ];
            $quantity  = $item[ 'quantity' ];

            $findBook = BookVariant ::whereBookId ( $productId ) -> whereId ( $variantId ) -> with (
                'book:id,title_bn,title_en,slug,image' , 'variantInfo'
            ) -> first ();

            $findBook -> buy_quantity = $item[ 'quantity' ];
            $findBook -> buy_price    = $findBook -> price - $findBook -> discount_value;
            $data[]                   = $findBook;

        }

        return $data;

    }

    /**
     * Get Cart Item Producct .
     *
     * @param  Array
     * @param  intiger productId, VendorID
     */

    public function getCartItemProduct ( Request $request )
    {
        $data = [];

        foreach ($request -> cart as $cart) {

            $productId = $cart[ 'product_id' ];
            $variantId = $cart[ 'variant_id' ];
            $quantity  = $cart[ 'quantity' ];

            $findBook = BookVariant ::whereBookId ( $productId ) -> whereId ( $variantId ) -> with (
                'book:id,title_bn,title_en,slug,image' , 'variantInfo'
            ) -> first ();

            $findBook -> buy_quantity = $cart[ 'quantity' ];
            $findBook -> buy_price    = $findBook -> price;
            $data[]                   = $findBook;

        }

        return $this -> respondCreated ( 'Cart  List' , $data );

    }


}
