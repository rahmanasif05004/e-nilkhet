<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class CustomerGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required',
            'status'=>'required',
        ];
        if(Request::input('discount.discount_type') || Request::input('discount.value')){
            $rules = [
                'discount.title'=>'required',
                'discount.discountable_type'=>'required',
                'discount.discount_type'=>'required',
                'discount.value'=>'required|numeric',
            ];
        }
        return $rules;
    }
}
