<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;
use App\Models\Book;
class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en'=>'required',
            'title_bn'=>'required',
            'categoriesIds'=>'required',
            'variants'=>'required|array',
            'variants.*.active_status'=>'required',
            'variants.*.price'=>'required|numeric',
            'variants.*.stock_qty'=>'required|numeric',
            'variants.*.variant_id'=>'required|numeric',
            'variants.*.discount_value'=>'required_with:variants.*.discount_type',
            'variants.*.discount_type'=>'required_with:variants.*.discount_value',
        ];
    }

    public function messages()
    {
        return[
            'variants.required'=>'Please insert at least one variant row'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator){
            $validateBook=Book::where(function($query){
                $query->where('title_en',request('titile_en'))->orWhere('title_bn',request('title_bn'));
            })->where('edition',request('edition'))->exists();
            if($validateBook){
                $validator->errors()->add('invalid_book', 'The book has already been inserted');
            }
        });

        return $validator;
        
    }
}
