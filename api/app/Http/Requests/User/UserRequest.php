<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Request::isMethod('post'))
        {
            $rules['email'] = 'required|email|unique:users,email';
            $rules['phone'] = 'required|unique:users,phone';
            $rules['password'] = 'required|min:8';
            // $rules['password_confirmation'] = 'required|same:password';
        } 
        else 
        {
            $rules['email'] = ['required', 'email', Rule::unique('users','email')->ignore(Request::input('id'),'id')];
            $rules['phone'] = ['required', Rule::unique('users','phone')->ignore(Request::input('id'),'id')];
            if(Request::input('password'))
            {
                $rules['password']='required|min:8';
                // $rules['password_confirmation'] = 'required|same:password';
            }
        }
        // if(Request::isMethod('patch')){
        //     $rules['email'] = ['required', 'email', Rule::unique('users','email')->ignore(Request::input('user.id'),'id')];
        // }else{
        //     $rules['email'] = 'required|email|unique:users,email';
        // }
        return $rules;
    }
}
