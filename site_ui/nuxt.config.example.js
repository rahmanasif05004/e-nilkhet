export default {
    head: {
        titleTemplate: 'Welcome to Enilkhet',
        title: 'Welcome to Enilkhet',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content:
                    'This is content'
            }
        ],
        link: [
            {
                rel: 'stylesheet',
                href:
                    'https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext'
            }
        ]
    },

    css: [
        'swiper/dist/css/swiper.css',
        '~/static/fonts/Linearicons/Font/demo-files/demo.css',
        '~/static/fonts/font-awesome/css/font-awesome.css',
        '~/static/css/bootstrap.min.css',
        '~/static/css/style.css',
        '~/assets/scss/style.scss'
    ],

    plugins: [
        { src: '~plugins/vueliate.js', ssr: false },
        { src: '~/plugins/swiper-plugin.js', ssr: false },
        { src: '~/plugins/vue-notification.js', ssr: false },
        { src: '~/plugins/axios.js', ssr: false },
        { src: '~/plugins/lazyLoad.js', ssr: false }
    ],

    buildModules: [
        '@nuxtjs/vuetify',
        '@nuxtjs/style-resources',
        'cookie-universal-nuxt'
    ],

    styleResources: {
        scss: './assets/scss/env.scss'
    },
    publicRuntimeConfig: {
        remoteBaseUrl: 'http://api.enilkhet.com.bd',
    },
    modules: ['@nuxtjs/axios', 'nuxt-i18n','@nuxtjs/auth-next'],
    auth:{
        strategies:{
            local: {
                token: {
                    property: 'access_token',
                    required: true,
                    type: 'Bearer',
                },
                user: {
                    property: 'data',
                    autoFetch: true
                },
                endpoints: {
                    login: { url: 'customer-login', method: 'post' },
                    user: { url: 'me', method: 'get' }
                }
            }


        },
        redirect:{
            login:'/account/login',
            logout:'/',
        }
    },
    axios: {
        baseURL: 'http://api.enilkhet.com.bd/api/v1/'
    },
    i18n: {
        locales: [
            { code: 'en', file: 'en.json' },
            { code: 'fr', file: 'fr.json' }
        ],
        lazy: true,
        defaultLocale: 'en',
        langDir: 'lang/locales/'
    },

    router: {
        linkActiveClass: '',
        linkExactActiveClass: 'active'
    },

    server: {
        port: 4002,
        host: 'localhost'
    }
};
