export const actions = {
  async nuxtServerInit({ state, commit, dispatch }) {
    //get cart info if user authenticate
    var response = await dispatch("cart/getCartList");
    if (response != undefined) {
      var cartData = response.data.data;
    } else {
      var cartItems = this.$cookies.get("cart", { parseJSON: true });
      if (cartItems && cartItems.cartItems.length > 0) {
        var cartData = cartItems.cartItems;
      } else {
        var cartData = [];
      }
    }
    // console.log(response);
    const wishlistItems = this.$cookies.get("wishlist", {
      parseJSON: true,
    });
    const compareItems = this.$cookies.get("compare", { parseJSON: true });
    const auth = this.$cookies.get("auth", { parseJSON: true });
    const currency = this.$cookies.get("currency", { parseJSON: true });

    if (cartData && cartData.length > 0) {
      commit("cart/initCart", {
        cartItems: cartData,
        total: cartData.length,
        amount: 0,
      });

      let cartItemIdInfo = [];
      cartData.forEach((item) => {
        cartItemIdInfo.push(item);
      });
      await dispatch("product/getCartProducts", cartItemIdInfo);
      await dispatch("cart/calculateAmount", state.product.cartProducts);
    }

    if (wishlistItems) {
      commit("wishlist/initWishlist", {
        items: wishlistItems.items,
        total: wishlistItems.total,
      });
    }
    if (compareItems) {
      commit("compare/initCompare", {
        items: compareItems.items,
        total: compareItems.total,
      });
    }
    if (auth) {
      commit("auth/setIsLoggedIn", Boolean(auth.isLoggedIn));
    }
    if (currency) {
      commit("app/setCurrency", currency.data);
    }
  },
};
