

export const state = () => ({
    variant: null
});

export const mutations = {
    setVariant(state, payload) {
        state.variant = payload;
    }
};

export const actions = {
    async setProductVariant({ commit }, payload) {
        let reponse = await commit('setVariant',payload);
        return reponse;
    }
};
