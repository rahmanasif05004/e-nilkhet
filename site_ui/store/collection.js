import Repository, {
  serializeQuery,
  baseUrl,
} from "~/repositories/Repository.js";

export const state = () => ({
  preOrderList: null,
  packageList:null,
  packageInfo:null,
  collections: null,
  categories: null,
  queries: null,
});

export const mutations = {
  setCollections(state, payload) {
    state.collections = payload;
  },
  setPreOrder(state, payload) {
    state.preOrderList = payload;
  },

  setCategories(state, payload) {
    state.categories = payload;
  },
  setQueries(state, payload) {
    state.queries = payload;
  },
  setPackageItems(state,payload){
    state.packageList = payload;
  },
  setPackageInfo(state,payload){
    state.packageInfo = state.packageList.find((item)=>{
      return item.slug == payload
    });
  }
};

export const actions = {
  async getCollections({ commit }, payload) {
    const reponse = await Repository.get(`${baseUrl}/book-parent-category-wise`)
      .then((response) => {
        commit("setCollections", response.data.data);
        return response.data.data;
      })
      .catch((error) => {
        commit("setCollections", []);
      });
    return reponse;
  },

  async getPreOrder({ commit }) {
    const reponse = await Repository.get(`${baseUrl}/books-preorder-list`)
      .then((response) => {
        commit("setPreOrder", response.data.data);
        return response.data.data;
      })
      .catch((error) => {
        commit("setPreOrder", []);
      });
    return reponse;
  },

  async packageList({ commit }) {
    const reponse = await Repository.get(`${baseUrl}/books-package-list`)
      .then((response) => {
        commit("setPackageItems", response.data.data);
        return response.data.data;
      })
      .catch((error) => {
        commit("setPackageItems", []);
      });
    return reponse;
  },

  async packageInfoBySlug({ commit },payload) {
    await commit("setPackageInfo", payload);
    return payload;
  },

  async getCategoriesBySlugs({ commit }, payload) {
    let query = "";
    payload.forEach((item) => {
      if (query === "") {
        query = `slug_in=${item}`;
      } else {
        query = query + `&slug_in=${item}`;
      }
    });
    const reponse = await Repository.get(
      `${baseUrl}/product-categories?${query}`
    )
      .then((response) => {
        commit("setCategories", response.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },
};
