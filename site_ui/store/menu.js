import Repository, { serializeQuery } from "~/repositories/Repository.js";
import { baseUrl } from "~/repositories/Repository";

export const state = () => ({
  mainMenu: [
    {
      name_en: "Author",
      url: "#",
      type: "author",
      subMenu: [],
    },
    {
      name_en: "Publisher",
      url: "#",
      type: "publisher",
      subMenu: [],
    },
    {
      name_en: "Editor",
      url: "#",
      type: "editor",
      subMenu: [],
    },
    {
      name_en: "Translator",
      url: "#",
      type: "translator",
      subMenu: [],
    },
  ],
  submenuList: [],
});

export const mutations = {
  setMenuItem(state, payload) {
    // state.submenuList = payload;
    state.mainMenu.filter((item) => {
      switch (item.type) {
        case "author":
          state.mainMenu[0].subMenu = payload.author;
          break;
        case "publisher":
          state.mainMenu[1].subMenu = payload.publisher;
          break;
        case "editor":
          state.mainMenu[2].subMenu = payload.editor;
          break;
        case "translator":
          state.mainMenu[3].subMenu = payload.translator;
          break;
      }
    });
  },
};

export const actions = {
  async setTopSubmenu({ commit }) {
    const reponse = await Repository.get(`${baseUrl}/sub-menu-item`)
      .then((response) => {
        commit("setMenuItem", response.data.data.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },
};
