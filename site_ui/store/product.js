import Repository, { serializeQuery } from "~/repositories/Repository.js";
import { baseUrl } from "~/repositories/Repository";

export const state = () => ({
  product: null,
  filterProdcuts: null,
  products: null,
  packageProducts:null,
  searchResults: null,
  cartProducts: null,
  wishlistItems: null,
  compareItems: null,
  brands: null,
  categories: null,
  total: 0,
});

export const mutations = {
  setProducts(state, payload) {
    state.products = payload;
    state.filterProdcuts = payload;
  },
  setProductsByRange(state, payload) {
    let rangeFilterProduct = state.filterProdcuts.filter((product) => {
      let productActivePrice =
        product.active_variant.price - product.active_variant.discount_value;
      if (
        productActivePrice >= payload.price_gt &&
        productActivePrice <= payload.price_lt
      ) {
        return product;
      }
    });
    state.products = rangeFilterProduct;
  },
  setCartProducts(state, payload) {
    state.cartProducts = payload;
  },
  setWishlistItems(state, payload) {
    state.wishlistItems = payload;
  },
  setCompareItems(state, payload) {
    state.compareItems = payload;
  },

  setProduct(state, payload) {
    state.product = payload;
  },

  setBrands(state, payload) {
    state.brands = payload;
  },

  setCategories(state, payload) {
    state.categories = payload;
  },

  setSearchResults(state, payload) {
    state.searchResults = payload;
  },

  setTotal(state, payload) {
    state.total = payload;
  },
  setPackageProduct(state,payload){
    state.packageProducts = payload;
  },
  removeItemFromPackage(state,payload){
    state.packageProducts = state.packageProducts.filter((item,idx)=>{
        return idx !== payload
    });
  }
};

export const actions = {
  async getProducts({ commit }, payload) {
    const response = await Repository.get(
      `${baseUrl}/products?${serializeQuery(payload)}`
    )
      .then((response) => {
        commit("setProducts", response.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return response;
  },

  async getTotalRecords({ commit }, payload) {
    const reponse = await Repository.get(`${baseUrl}/products/count`)
      .then((response) => {
        commit("setTotal", response.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async getProductsBySlug({ commit }, payload) {
    const reponse = await Repository.get(`${baseUrl}/book-deatails/${payload}`)
      .then((response) => {
        commit("setProduct", response.data.data.book);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async getPackageProduct({ commit }, payload) {
    const reponse = await Repository.get(`${baseUrl}/book-package-list-by-slug/${payload}`)
      .then((response) => {
        commit("setPackageProduct", response.data.data.collections.books);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async removeItemFromPackageList({ commit }, payload){
    await commit("removeItemFromPackage", payload);
  },

  async getProductByKeyword({ commit }, payload) {
    const reponse = await Repository.get(
      `${baseUrl}/products?${serializeQuery(payload)}`
    )
      .then((response) => {
        commit("setSearchResults", response.data);
        commit("setTotal", response.data.length);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },
  async saveCartProductList({ commit }, payload) {
    commit("setCartProducts", payload);
  },
  async getCartProducts({ commit }, payload) {
    const reponse = await Repository.post(`${baseUrl}/cart-products`, {
      cart: payload,
    })
      .then((response) => {
        commit("setCartProducts", response.data.data);
        return response.data.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async getWishlishtProducts({ commit }, payload) {
    const reponse = await Repository.post(`${baseUrl}/book-info`, {
      productList: payload,
    })
      .then((response) => {
        console.log(reponse);
        commit("setWishlistItems", response.data.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async getCompareProducts({ commit }, payload) {
    let query = "";
    payload.forEach((item) => {
      if (query === "") {
        query = `id=${item}`;
      } else {
        query = query + `&id=${item}`;
      }
    });
    const reponse = await Repository.get(`${baseUrl}/products?${query}`)
      .then((response) => {
        commit("setCompareItems", response.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async getProductBrands({ commit }) {
    const reponse = await Repository.get(`${baseUrl}/brands`)
      .then((response) => {
        commit("setBrands", response.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async getProducCategoryWise({ commit }, payload) {
    const reponse = await Repository.get(`${baseUrl}/books-list/${payload}`)
      .then((response) => {
        // commit('setCategories', response.data.data.categories);
        commit("setProducts", response.data.data.collections);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },
  async getProductsForMenu({ commit }, payload) {
    const reponse = await Repository.get(
      `${baseUrl}/sub-menu-product/${payload.type}/${payload.id}`
    )
      .then((response) => {
        commit("setProducts", response.data.data.collections);
        return response.data;
      })
      .catch((error) => {
        commit("setProducts", []);
      });
    return reponse;
  },

  async getShopProductList({ commit }, payload = null) {
    const reponse = await Repository.get(`${baseUrl}/shop-product/` + payload)
      .then((response) => {
        commit("setProducts", response.data.data.collections);
        return response.data;
      })
      .catch((error) => {
        commit("setProducts", []);
      });
    return reponse;
  },

  async getCategoryForSticky({ commit }, payload) {
    const reponse = await Repository.get(
      `${baseUrl}/sticky-menu-categories/${payload}`
    )
      .then((response) => {
        commit("setCategories", response.data.data);
        return response.data;
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },

  async getProductsByBrands({ commit }, payload) {
    let query = "";
    payload.forEach((item) => {
      if (query === "") {
        query = `slug_in=${item}`;
      } else {
        query = query + `&slug_in=${item}`;
      }
    });
    const reponse = await Repository.get(`${baseUrl}/brands?${query}`)
      .then((response) => {
        if (response.data) {
          const brands = response.data;
          let products = [];
          brands.forEach((brand) => {
            brand.products.forEach((product) => {
              products.push(product);
            });
          });
          commit("setProducts", products);
          commit("setTotal", products.length);
          return products;
        } else {
          return null;
        }
      })
      .catch((error) => ({ error: JSON.stringify(error) }));
    return reponse;
  },
  async getProductsByPriceRange({ commit }, payload) {
    commit("setProductsByRange", payload);
    // const response = await Repository.get(
    //     `${baseUrl}/products?${serializeQuery(payload)}`
    // )
    //     .then(response => {
    //         commit('setProducts', response.data);
    //         commit('setSearchResults', response.data);
    //         commit('setTotal', response.data.length);
    //         return response.data;
    //     })
    //     .catch(error => ({ error: JSON.stringify(error) }));

    return true;
  },
};
