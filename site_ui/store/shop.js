import data from "~/static/data/products.json";

export const state = () => ({
  products: data.data,
  discount: {
    occasion_discount_name: "",
    occasion_discount: 0,
    customer_discount: 0,
  },
  selected_delivery_item: null,
  delivery_info: [],
});

export const getters = {
  getAllProducts: (state) => {
    return state.products;
  },
  getFeaturedProducts: (state) =>
    state.products.filter((el) => el.feature === true),
};

export const mutations = {
  setDiscount(state, payload) {
    state.discount.customer_discount = payload.customerDiscount;
    state.discount.occasion_discount = payload.occasionDiscount
      ? payload.occasionDiscount.value
      : 0;
    state.discount.occasion_discount_name = payload.occasionDiscount
      ? payload.occasionDiscount.title
      : "";
    state.delivery_info = payload.deliveryInfo;
  },
  setDeliveryItem(state, payload) {
    state.selected_delivery_item = payload;
  },
};

export const actions = {
  async setOrderDiscount({ commit }) {
    var response = await this.$axios.get("active-discounts");
    let reponse = await commit("setDiscount", response.data.data);
    return response;
  },

  async setDeliveryItem({ commit }, payload) {
    await commit("setDeliveryItem", payload);
  },
};
