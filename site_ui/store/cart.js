const calculateAmount = (obj) =>
  Object.values(obj)
    .reduce(
      (acc, { buy_quantity, buy_price }) => acc + buy_quantity * buy_price,
      0
    )
    .toFixed(2);

export const state = () => ({
  total: 0,
  amount: 0,
  cartItems: [],
  loading: false,
});

export const mutations = {
  initCart(state, payload) {
    state.cartItems = payload.cartItems;
    state.amount = payload.amount;
    state.total = payload.total;
  },

  setLoading(state, payload) {
    state.loading = payload;
  },

  addItem(state, payload) {
    state.cartItems = [];
    console.log(payload);
    payload.forEach((element) => {
      state.cartItems.push(element);
    });
    state.total = payload.length;
    state.amount = 0;
  },

  removeItem: (state, payload) => {
    const index = state.cartItems.findIndex(
      (item) =>
        item.product_id === payload.product_id &&
        item.variant_id === payload.variant_id
    );
    state.cartItems.splice(index, 1);
    state.total = state.total - 1;
    if (state.cartItems.length === 0) {
      state.cartItems = [];
      state.amount = 0;
      state.total = 0;
    }
  },

  increaseItemQuantity(state, payload) {
    const index = payload.index;

    if (index !== -1) {
      state.cartItems[index].quantity++;
    }
  },

  decreaseItemQuantity(state, payload) {
    const index = payload.index;
    if (index !== -1) {
      state.cartItems[index].quantity--;
    }
  },

  computedTotalAmount(state, payload) {
    state.amount = calculateAmount(payload);
  },

  clearCart: (state) => {
    state.cartItems = [];
    state.amount = 0;
    state.total = 0;
  },
};

export const actions = {
  async addProductToCart({ commit, state }, payload) {
    await commit("addItem", payload);
    const cookieParams = {
      total: state.total,
      amount: state.amount,
      cartItems: state.cartItems,
    };
    this.$cookies.set("cart", cookieParams, {
      path: "/",
      maxAge: 60 * 60 * 24 * 7,
    });
  },

  async removeProductFromCart({ commit, state }, payload) {
    commit("removeItem", payload);
    const cookieParams = {
      total: state.total,
      amount: state.amount,
      cartItems: state.cartItems,
    };

    this.$cookies.set("cart", cookieParams, {
      path: "/",
      maxAge: 60 * 60 * 24 * 7,
    });
  },

  increaseCartItemQuantity({ commit, state }, payload) {
    //check auth then logic is change
    commit("increaseItemQuantity", payload);
    const cookieParams = {
      total: state.total,
      amount: state.amount,
      cartItems: state.cartItems,
    };

    this.$cookies.set("cart", cookieParams, {
      path: "/",
      maxAge: 60 * 60 * 24 * 7,
    });
  },

  decreaseCartItemQuantity({ commit, state }, payload) {
    //check auth then logic is change
    commit("decreaseItemQuantity", payload);
    const cookieParams = {
      total: state.total,
      amount: state.amount,
      cartItems: state.cartItems,
    };

    this.$cookies.set("cart", cookieParams, {
      path: "/",
      maxAge: 60 * 60 * 24 * 7,
    });
  },
  calculateAmount({ commit, state }, payload) {
    //check auth then logic is change
    commit("computedTotalAmount", payload);
  },
  clearCart({ commit }) {
    //check auth then logic is change
    commit("clearCart");
    const cookieParams = {
      total: 0,
      amount: 0,
      cartItems: [],
    };
    this.$cookies.set("cart", cookieParams, {
      path: "/",
      maxAge: 60 * 60 * 24 * 7,
    });
  },

  async getCartList() {
    try {
      var response = await this.$axios.get("/get-cart-info");
      return response;
    } catch (error) {
      // console.log(error);
    }
  },
};
