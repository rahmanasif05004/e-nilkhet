import Cookies from "js-cookie";
import axios from "axios";

const token = Cookies.get("id_token");
// const baseDomain = 'http://45.76.97.89:3000';
// const baseDomain = "http://api.enilkhet.com.bd/api/v1";
// const baseDomainPublic = "http://api.enilkhet.com.bd/";

const baseDomain = "http://localhost:8000/api/v1";
const baseDomainPublic = "http://localhost:8000/";

export const customHeaders = {
  "Content-Type": "application/json",
  Accept: "application/json",
};

export const baseUrl = `${baseDomain}`;
export const baseUrlPublic = `${baseDomainPublic}`;

export default axios.create({
  baseUrl,
  headers: customHeaders,
});

export const serializeQuery = (query) => {
  return Object.keys(query)
    .map(
      (key) => `${encodeURIComponent(key)}=${encodeURIComponent(query[key])}`
    )
    .join("&");
};
