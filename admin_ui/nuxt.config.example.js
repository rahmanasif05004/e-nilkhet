const webpack = require('webpack')
export default {
  mode: "universal",
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'enillkhet',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'},
      { src: "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js", body: true },
      { src: "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js", body: true },
      // { src: 'https://pratikborsadiya.in/vali-admin/js/main.js', body: true },
      { src: 'main.js', body: true },

    ],
    link: [

      {
        rel : 'stylesheet',
        href:"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      },
       { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  router: {
    middleware: [
      'validationClear',
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '~/assets/css/main.css',
    "~/node_modules/bootstrap/dist/css/bootstrap.css",
  ],


  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    './plugins/mixins/validation',
    './plugins/mixins/user',
    './plugins/axios',
    './plugins/common',
    { src: '~/plugins/external-packages.js', ssr: false }
  ],
  env: {
    baseUrl: 'http://api.enilkhet.com.bd/api/v1/',
    imageBaseUrl:'http://api.enilkhet.com.bd/',
  },
  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    "vue-toastification/nuxt",
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'vue-sweetalert2/nuxt'

  ],
  toast: {
    timeout: 2000,
    closeOnClick: false
  },
  axios: {
    baseURL: 'http://api.enilkhet.com.bd/api/v1/'
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: 'admin-login', method: 'post', propertyName: 'access_token' },
          user:{ url: 'me', method: 'get', propertyName: 'data' },
          logout: { method: 'post', url: 'logout' }
        }
      }
    },
    redirect:{
      login:'/login',
      logout:'/',
    },
    plugins: [
      // './plugins/auth'
    ]
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  /*
   ** Build configuration
   */
  build: {
  },
}
