import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor';
import Multiselect from 'vue-multiselect'
import datePicker from 'vue-bootstrap-datetimepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';

Vue.use(require('vue-moment'));
Vue.component('multiselect', Multiselect)


Vue.use(datePicker);

Vue.use(VueQuillEditor);
