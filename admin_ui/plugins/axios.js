export default function({ $axios, store }) {
    $axios.onError(error => {
        if (error.response.status == 422 || error.response.status == 401 ||  error.response.status == 500) {
            store.commit('commonStore/VALIDATION_ERROR', error);
        }
        console.log("hell");
        return Promise.reject(error);
    });

    $axios.onRequest(() => {
        store.commit('commonStore/CLEAR_VALIDATION');
    });
    //to set up axios interceptors
    $axios.interceptors.request.use(
        config => {
            store.commit("commonStore/SHOW_LOADER");
            return config;
        },
        error => {
            store.commit("commonStore/HIDE_LOADER");
            return Promise.reject(error);
        }
    );
    $axios.interceptors.response.use(
        response => {
            store.commit("commonStore/HIDE_LOADER");
            return response;
        },
        error => {
            store.commit("commonStore/HIDE_LOADER");
            return Promise.reject(error);
        }
    );
}
