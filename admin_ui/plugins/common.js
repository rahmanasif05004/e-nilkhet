export default ({app,$axios,store},inject)=>{
  inject('validationError',(key)=>{
    let errors=store.getters['commonStore/validationErrors'];
    if (errors == null) {
      return null;
    } else if (errors[key]) {
        return errors[key].join();
    } else {
        return "";
    }

  });
}
