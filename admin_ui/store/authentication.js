// export const state = () => ({
//     busy: false,
//     loggedIn: false,
//     strategy: "local",
//     user: false,
//     showLoader: false,
//     validationErrors: [],
//     serverError: false
// });

// export const getters = {
//     authenticated(state) {
//         return state.loggedIn;
//     },
//     user(state) {
//         return state.user;
//     },
//     //validation realted
//     validationErrors: state => {
//         return state.validationErrors;
//     },
//     serverError: state => {
//         return state.serverError;
//     }
// };

// export const mutations ={
//     SET_AUTH_INFO(state, status) {
//         state.loggedIn = status;
//         state.user = status;
//     },
//     SHOW_LOADER(state) {
//         state.showLoader = true;
//     },
//     HIDE_LOADER(state) {
//         state.showLoader = false;
//     },

//     //validation realted
//     VALIDATION_ERROR(state, error) {
//         if (error.response.status === 422) {
//             state.validationErrors = error.response.data.errors;
//         } else if (error.response.status === 401) {
//             state.serverError = error.response.data.message;
//         } else if (error.response.status === 500) {
//             state.serverError = "Sorry Server Error";
//         }
//     },
//     CLEAR_VALIDATION(state) {
//         state.validationErrors = [];
//         state.serverError = false;
//     }
// }

// export const actions ={
//     setStatus({ commit }, status) {
//         commit('SET_AUTH_INFO', status);
//     },
//     //validation realtiong action
//     clearValidation({ commit },status) {
//         commit('CLEAR_VALIDATION');
//     }
// }
